<?php session_start();
	include ("inc/db.php");
	
	if(!isset($_SESSION['gebruikersnaam'])) {
		header('Location: index.php');
	}?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>LVS | Studenten</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no">
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/stuck.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/ihover.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.1.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/tmStickUp.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script>
			$(document).ready(function(){
			 $().UItoTop({ easingType: 'easeOutQuart' });
			 $('#stuck_container').tmStickUp({});
			 });
		</script>
		<!--[if lt IE 9]>
		<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
			</a>
		</div>
		<script src="js/html5shiv.js"></script>
		<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
		<!--[if lt IE 10]>
		<link rel="stylesheet" media="screen" href="css/ie1.css">
		<![endif]-->
	</head>
	<body class="page1" id="top">
		<!--==============================
			header
			=================================-->
		<header>
			<!--==============================
				Stuck menu
				=================================-->
			<?php include ("inc/header.php"); ?>
		</header>
		<!--=====================
			Content
			======================-->
		<section class="content">
			<div class="ic">More Website Templates @ TemplateMonster.com - June 16, 2014!</div>
			<center>
				<div class="container">
					
						<?php $i = 1;
						if($_SESSION['bevid'] == 3) {
							if(isset($_GET['klasid'])) {
								$klasid2 = $_GET['klasid'];
								$query_student2 = "SELECT * FROM studenten INNER JOIN studenten_details ON studenten.studid = studenten_details.studid WHERE klasid='$klasid2'";
								$result_student2 = mysqli_query($db, $query_student2);
								
								$query_klas = "SELECT * FROM klassen WHERE klasid='$klasid2'";
								$result_klas = mysqli_query($db, $query_klas);
								$data_klas = mysqli_fetch_array($result_klas); 
								?>
								<p>
								<h1 style="font-size: 30px;">Studenten van <?php echo $data_klas['klas'];?></h1>
								</p>
								<table border="1">
									<tr>
										<th>ID</th>
										<th>Naam</th>
										<th>Voornaam</th>
										<th>Studentemail</th>
										<th>Klas</th>
										<th>Inschrijvingsjaar</th>
									</tr>
								<?php
								while($data_student2 = mysqli_fetch_array($result_student2)) {
									$studdetid = $data_student2['studdetid'];
									$sjid = $data_student2['sjid'];
									$query_schooljaar = "SELECT * FROM schooljaar WHERE sjid='$sjid'";
									$result_schooljaar = mysqli_query($db, $query_schooljaar);
									$data_schooljaar = mysqli_fetch_array($result_schooljaar);
									?>
									<tr>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $i;?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['voornaam'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['naam'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['email'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_klas['klas'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_schooljaar['schooljaar'];?></a></td>
									</tr>
									<?php $i++;
								}
							} else {
								?>
								<p>
								<h1 style="font-size: 30px;">Studenten van <?php echo $_SESSION['volNaam'];?></h1>
								</p>
								<table border="1">
									<tr>
										<th>ID</th>
										<th>Naam</th>
										<th>Voornaam</th>
										<th>Studentemail</th>
										<th>Klas</th>
										<th>Inschrijvingsjaar</th>
									</tr>
								<?php
								foreach($_SESSION as $name => $value) {
									if(substr($name, 0, 7) == 'klasid_') {
										$klasid = $value;
										$query_student = "SELECT * FROM studenten INNER JOIN studenten_details ON studenten.studid = studenten_details.studid WHERE klasid='$klasid'";
										$result_student = mysqli_query($db, $query_student);
										while($data_student = mysqli_fetch_array($result_student)) {
											$studdetid = $data_student['studdetid'];
											$sjid = $data_student['sjid'];
											$query_schooljaar = "SELECT * FROM schooljaar WHERE sjid='$sjid'";
											$result_schooljaar = mysqli_query($db, $query_schooljaar);
											$data_schooljaar = mysqli_fetch_array($result_schooljaar);
											
											$query_klas = "SELECT * FROM klassen WHERE klasid='$klasid'";
											$result_klas = mysqli_query($db, $query_klas);
											$data_klas = mysqli_fetch_array($result_klas);?>
											<tr>
												<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $i;?></a></td>
												<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student['voornaam'];?></a></td>
												<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student['naam'];?></a></td>
												<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student['email'];?></a></td>
												<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_klas['klas'];?></a></td>
												<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_schooljaar['schooljaar'];?></a></td>
											</tr>
										<?php $i++;
										}
									}
								}
							}
						} else if($_SESSION['bevid'] == 4) {
							if(isset($_GET['klasid'])) {
								$klasid2 = $_GET['klasid'];
								$query_student2 = "SELECT * FROM studenten INNER JOIN studenten_details ON studenten.studid = studenten_details.studid WHERE klasid='$klasid2'";
								$result_student2 = mysqli_query($db, $query_student2);
								
								$query_klas = "SELECT * FROM klassen WHERE klasid='$klasid2'";
								$result_klas = mysqli_query($db, $query_klas);
								$data_klas = mysqli_fetch_array($result_klas); 
								?>
								<p>
								<h1 style="font-size: 30px;">Studenten van <?php echo $data_klas['klas'];?></h1>
								</p>
								<table border="1">
									<tr>
										<th>ID</th>
										<th>Naam</th>
										<th>Voornaam</th>
										<th>Studentemail</th>
										<th>Klas</th>
										<th>Inschrijvingsjaar</th>
									</tr>
								<?php
								while($data_student2 = mysqli_fetch_array($result_student2)) {
									$studdetid = $data_student2['studdetid'];
									$sjid = $data_student2['sjid'];
									$query_schooljaar = "SELECT * FROM schooljaar WHERE sjid='$sjid'";
									$result_schooljaar = mysqli_query($db, $query_schooljaar);
									$data_schooljaar = mysqli_fetch_array($result_schooljaar);
									?>
									<tr>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $i;?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['voornaam'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['naam'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['email'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_klas['klas'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_schooljaar['schooljaar'];?></a></td>
									</tr>
									<?php $i++;
								}
							} else {
								$secid = $_SESSION['secid'];
								$query_student = "SELECT * FROM klassen INNER JOIN studenten_details ON klassen.klasid = studenten_details.klasid INNER JOIN studenten ON studenten_details.studid = studenten.studid WHERE klassen.secid = '$secid'";
								$result_student = mysqli_query($db, $query_student);
								?>
								<p>
								<h1 style="font-size: 30px;">Studenten van sector <?php echo $_SESSION['sector'];?></h1>
								</p>
								<table border="1">
									<tr>
										<th>ID</th>
										<th>Naam</th>
										<th>Voornaam</th>
										<th>Studentemail</th>
										<th>Klas</th>
										<th>Inschrijvingsjaar</th>
									</tr>
								<?php
								while($data_student = mysqli_fetch_array($result_student)) {
									$studdetid = $data_student['studdetid'];
									$sjid = $data_student['sjid'];
									$query_schooljaar = "SELECT * FROM schooljaar WHERE sjid='$sjid'";
									$result_schooljaar = mysqli_query($db, $query_schooljaar);
									$data_schooljaar = mysqli_fetch_array($result_schooljaar);?>
									<tr>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $i;?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student['voornaam'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student['naam'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student['email'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student['klas'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_schooljaar['schooljaar'];?></a></td>
									</tr>
									<?php $i++;
								}
							}
						} else if($_SESSION['bevid'] == 5 || $_SESSION['bevid'] == 6) {
							if(isset($_GET['klasid'])) {
								$klasid2 = $_GET['klasid'];
								$query_student2 = "SELECT * FROM studenten INNER JOIN studenten_details ON studenten.studid = studenten_details.studid WHERE klasid='$klasid2'";
								$result_student2 = mysqli_query($db, $query_student2);
								
								$query_klas = "SELECT * FROM klassen WHERE klasid='$klasid2'";
								$result_klas = mysqli_query($db, $query_klas);
								$data_klas = mysqli_fetch_array($result_klas); 
								?>
								<p>
								<h1 style="font-size: 30px;">Studenten van <?php echo $data_klas['klas'];?></h1>
								</p>
								<table border="1">
									<tr>
										<th>ID</th>
										<th>Naam</th>
										<th>Voornaam</th>
										<th>Studentemail</th>
										<th>Klas</th>
										<th>Inschrijvingsjaar</th>
									</tr>
								<?php
								while($data_student2 = mysqli_fetch_array($result_student2)) {
									$studdetid = $data_student2['studdetid'];
									$sjid = $data_student2['sjid'];
									$query_schooljaar = "SELECT * FROM schooljaar WHERE sjid='$sjid'";
									$result_schooljaar = mysqli_query($db, $query_schooljaar);
									$data_schooljaar = mysqli_fetch_array($result_schooljaar);
									?>
									<tr>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $i;?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['voornaam'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['naam'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['email'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_klas['klas'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_schooljaar['schooljaar'];?></a></td>
									</tr>
									<?php $i++;
								}
							} else {
								$query_student2 = "SELECT * FROM studenten INNER JOIN studenten_details ON studenten.studid = studenten_details.studid INNER JOIN klassen ON studenten_details.klasid = klassen.klasid";
								$result_student2 = mysqli_query($db, $query_student2);
								?>
								<p>
								<h1 style="font-size: 30px;">Alle Studenten</h1>
								</p>
								<table border="1">
									<tr>
										<th>ID</th>
										<th>Naam</th>
										<th>Voornaam</th>
										<th>Studentemail</th>
										<th>Klas</th>
										<th>Inschrijvingsjaar</th>
									</tr>
								<?php
								while($data_student2 = mysqli_fetch_array($result_student2)) {
									$studdetid = $data_student2['studdetid'];
									$sjid = $data_student2['sjid'];
									$query_schooljaar = "SELECT * FROM schooljaar WHERE sjid='$sjid'";
									$result_schooljaar = mysqli_query($db, $query_schooljaar);
									$data_schooljaar = mysqli_fetch_array($result_schooljaar);
									?>
									<tr>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $i;?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['voornaam'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['naam'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['email'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_student2['klas'];?></a></td>
										<td><a href="studentenkaart.php?studdetid=<?php echo $studdetid;?>"><?php echo $data_schooljaar['schooljaar'];?></a></td>
									</tr>
									<?php $i++;
								}
							}
						}?>
					</table>
					<br/>
				</div>
			</center>
		</section>
		<!--==============================
			footer
			=================================-->
		<?php include ("inc/footer.php"); ?>
	</body>
</html>