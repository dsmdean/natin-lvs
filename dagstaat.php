<?php session_start();
	include ("inc/db.php");
	
	if(!isset($_SESSION['gebruikersnaam'])) {
		header('Location: index.php');
	}
	
	if(isset($_POST['submitDagstaat'])) {
		$klasid = $_SESSION['klasid'];
		$week = $_SESSION['week'];
		$dag = $_SESSION['dag'];
		$datum = $_SESSION['datum'];
		$ko = $_SESSION['ko'];
		$nietGevolgd = array();
		for($i=1;$i<9;$i++) {
			${"doc_".$i} = $_SESSION['doc_'.$i];
			${"GevolgdLesuur_".$i} = $_SESSION['GevolgdLesuur'.$i];
			
			if(${"GevolgdLesuur_".$i} == 2) {
				//$nietGevolgd = $i;
				array_push($nietGevolgd, $i);
			}
		}
		
		for($i=1;$i<9;$i++) {
			if(!empty($_POST['afspraak_'.$i])) {
				${"afspraak_".$i} = $_POST['afspraak_'.$i];
			} else {
				${"afspraak_".$i} = "";
			}
			if(!empty($_POST['behandeld_'.$i])) {
				${"behandeld_".$i} = $_POST['behandeld_'.$i];
			} else {
				${"behandeld_".$i} = "";
			}
			
			${"vak_".$i} = $_SESSION['vak_'.$i];
		}
		
		$query_insert_pres = "INSERT INTO `presentie` (`klasid`, `week`, `dag`, `datum`, `studid`, `docid_1`, `les_1`, `vak_1`, `docid_2`, `les_2`, `vak_2`, `docid_3`, `les_3`, `vak_3`, `docid_4`, `les_4`, `vak_4`, `docid_5`, `les_5`, `vak_5`, `docid_6`, `les_6`, `vak_6`, `docid_7`, `les_7`, `vak_7`, `docid_8`, `les_8`, `vak_8`) 
								VALUES ('$klasid', '$week', '$dag', '$datum', '$ko', '$doc_1', '$GevolgdLesuur_1', '$vak_1', '$doc_2', '$GevolgdLesuur_2', '$vak_2', '$doc_3', '$GevolgdLesuur_3', '$vak_3', '$doc_4', '$GevolgdLesuur_4', '$vak_4', '$doc_5', '$GevolgdLesuur_5', '$vak_5', '$doc_6', '$GevolgdLesuur_6', '$vak_6', '$doc_7', '$GevolgdLesuur_7', '$vak_7', '$doc_8', '$GevolgdLesuur_8', '$vak_8')";
		$result_insert_pres = mysqli_query($db, $query_insert_pres);
				
		if(mysqli_affected_rows($db) == 1) {
			$presid = mysqli_insert_id($db);
			mysqli_close($db);
			
			for($i=0;$i<count($nietGevolgd);$i++) {
				$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
				$query_insert_nietverzorgd = "INSERT INTO `niet_verzorgd` (`presid`, `lesuur`) 
											VALUES ('$presid', '$nietGevolgd[$i]')";
				$result_insert_nietverzorgd = mysqli_query($db, $query_insert_nietverzorgd);
				mysqli_close($db);
			}
			
			$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			$query_insert_behandeld = "INSERT INTO `behandeld` (`presid`, `behandeld_1`, `behandeld_2`, `behandeld_3`, `behandeld_4`, `behandeld_5`, `behandeld_6`, `behandeld_7`, `behandeld_8`) 
										VALUES ('$presid', '$behandeld_1', '$behandeld_2', '$behandeld_3', '$behandeld_4', '$behandeld_5', '$behandeld_6', '$behandeld_7', '$behandeld_8')";
			$result_insert_behandeld = mysqli_query($db, $query_insert_behandeld);
			mysqli_close($db);
			
			$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			$query_insert_afspraken = "INSERT INTO `afspraken` (`presid`, `afspraak_1`, `afspraak_2`, `afspraak_3`, `afspraak_4`, `afspraak_5`, `afspraak_6`, `afspraak_7`, `afspraak_8`) 
										VALUES ('$presid', '$afspraak_1', '$afspraak_2', '$afspraak_3', '$afspraak_4', '$afspraak_5', '$afspraak_6', '$afspraak_7', '$afspraak_8')";
			$result_insert_afspraken = mysqli_query($db, $query_insert_afspraken);
			mysqli_close($db);
			
			foreach($_SESSION as $name => $value) {
				if(substr($name, 0, 8) == 'student_') {
					$id = substr($name, 8, (strlen($name)-8));
					for($i=1;$i<9;$i++) {
						if(isset($_POST['les_'.$i.'_'.$id])) {
							${"les_".$i} = $_POST['les_'.$i.'_'.$id];
						} else {
							${"les_".$i} = 2;
						}
					}
					$student = $_SESSION['student_'.$id];
					
					$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
					$query_insert_pres_det = "INSERT INTO `presentie_details` (`presid`, `studid`, `les_1`, `les_2`, `les_3`, `les_4`, `les_5`, `les_6`, `les_7`, `les_8`) 
												VALUES ('$presid', '$student', '$les_1', '$les_2', '$les_3', '$les_4', '$les_5', '$les_6', '$les_7', '$les_8')";
					$result_insert_pres_det = mysqli_query($db, $query_insert_pres_det);
					mysqli_close($db);
				}
			}
		}
		header('Location: rc.php');
	}?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>LVS | Index</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no">
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/stuck.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/ihover.css">
		<script type="text/javascript" src="js/css-pop.js"></script>
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.1.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/tmStickUp.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script>
			$(document).ready(function(){
			 $().UItoTop({ easingType: 'easeOutQuart' });
			 $('#stuck_container').tmStickUp({});
			 });
		</script>
		<!--[if lt IE 9]>
		<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
			</a>
		</div>
		<script src="js/html5shiv.js"></script>
		<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
		<!--[if lt IE 10]>
		<link rel="stylesheet" media="screen" href="css/ie1.css">
		<![endif]-->
	</head>
	<body class="page1" id="top" onload="popup('popUpDiv')">
		<!--==============================
			header
			=================================-->
		<header>
			<!--==============================
				Stuck menu
				=================================-->
			<?php include ("inc/header.php"); ?>
		</header>
		<!--=====================
			Content
			======================-->
			<?php
			$docentid = $_SESSION['gebruikersid'];
			$query_dagstaat = "SELECT * FROM klassen WHERE docid='$docentid'";
			$result_dagstaat = mysqli_query($db, $query_dagstaat);
			
			if(!isset($_GET['klas']) && !isset($_GET['next'])) {?>
				<div id="blanket" style="display:none;"></div>
				<div id="popUpDiv" style="display:none; top:250px;">			
				<form action="dagstaat.php" method="GET">
					<div style="background:rgb(16, 70, 16); color: white;font-size: 20px;padding: 10px;font-weight: bold;"> <center>Kies een klas</center> </div>	
					<center>
						<div style="margin-top: 20px;">Dagstaat Invullen voor Klas: 
							<select name="klas">
								<?php
								while($data_dagstaat = mysqli_fetch_array($result_dagstaat)) {?>
									<option value="<?php echo $data_dagstaat['klasid']; ?>"><strong><?php echo $data_dagstaat['klas']; ?></strong></option>
								<?php
								}
								?>
							</select>
						</div>
					</center>
					<div style ="float: right; margin-right:50px; margin-top: 10px;"> 
						<input type="submit" value="Kiezen" onclick="popup('popUpDiv')" style="width: 70px; height: 35px; padding-left: 20px;padding-right: 20px;padding-top: 5px;padding-bottom: 5px;background: rgb(16, 70, 16); color: white;border: 0px;" >
					</div>			
				</form>
				</div>	
			<?php
			}?>
			
		<?php
			if(!isset($_SESSION['klasid'])) {
				$klasid = $_GET['klas'];
				$_SESSION['klasid'] = $klasid;
			} else if(isset($_GET['klas'])){
				$klasid = $_GET['klas'];
				$_SESSION['klasid'] = $klasid;
			} else {
				$klasid = $_SESSION['klasid'];
			}
			$query_student = "SELECT * FROM studenten INNER JOIN studenten_details ON studenten.studid = studenten_details.studid WHERE klasid='$klasid'";
			$result_student = mysqli_query($db, $query_student);
			//$data_student = mysqli_fetch_array($result_student);
			$dates = getdate();
			$query_docent = "SELECT * FROM docenten WHERE docid='$docentid'";
			$result_docent = mysqli_query($db, $query_docent);
			$data_docent = mysqli_fetch_array($result_docent);
			
			$query_klas = "SELECT * FROM klassen WHERE klasid='$klasid'";
			$result_klas  = mysqli_query($db, $query_klas);
			$data_klas = mysqli_fetch_array($result_klas);
			?>
		<section class="content">
			<div class="ic">More Website Templates @ TemplateMonster.com - June 16, 2014!</div>
			<center>
				<div class="container">
					<p>
					<h1 style="font-size: 30px;">DAGSTAAT VERZORGDE LESSEN SCHOOLJAAR 2013/2014 </h1>
					</p>
					<hr>
					<!--==============================
						Na NEXT
						=================================-->
					<?php 
					if(isset($_GET['next'])) {
						$week = $_GET['week'];
						$_SESSION['week'] = $week;
						if(isset($_GET['dag'])) {
							$dag = $_GET['dag'];
							$_SESSION['dag'] = $dag;
						} else {
							$dag = $_SESSION['dag'];
						}
						if(isset($_GET['datum'])) {
							$datum = $_GET['datum'];
							$_SESSION['datum'] = $datum;
						} else {
							$datum = $_SESSION['datum'];
						}
						$ko = $_GET['ko'];
						$_SESSION['ko'] = $ko;
						$klasid = $_SESSION['klasid'];
						
						for($i=1;$i<9;$i++) {
							${"vak_".$i} = $_GET['vak_'.$i];
							$_SESSION['vak_'.$i] = ${"vak_".$i};
							${"doc_".$i} = $_GET['doc_'.$i];
							$_SESSION['doc_'.$i] = ${"doc_".$i};
							${"GevolgdLesuur_".$i} = $_GET['GevolgdLesuur'.$i];
							$_SESSION['GevolgdLesuur'.$i] = ${"GevolgdLesuur_".$i};
						}
						
						$query_ko = "SELECT * FROM studenten INNER JOIN studenten_details ON studenten.studid = studenten_details.studid WHERE klasid='$klasid' AND studdetid='$ko'";
						$result_ko = mysqli_query($db, $query_ko);
						$data_ko = mysqli_fetch_array($result_ko);
						?>
					<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
						Klas: <strong><?php echo $data_klas['klas'];?></strong> &nbsp;&nbsp;&nbsp; 
						<strong>A.O</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						Week: <strong><?php echo $week;?></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						Dag: <strong><?php echo $dag;?></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						Datum: <strong><?php echo $datum;?></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						Naam KO: <strong><?php echo $data_ko['voornaam']." ".$data_ko['naam'];?></strong> &nbsp;&nbsp;&nbsp; 
						Klassendocent: <strong><?php echo $data_docent['voornaam']." ".$data_docent['naam'];?></strong>
						<br><br>
						<div style ="float:left;">
							<style type="text/css">
								.tg  {border-collapse:collapse;border-spacing:0;}
								.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg .tg-s6z2{text-align:center}
								.tg .tg-e3zv{font-weight:bold}
								.tg .tg-hgcj{font-weight:bold;text-align:center}
							</style>
							<table class="tg" style="undefined;table-layout: fixed; width: 638px">
								<colgroup>
									<col style="width: 52px">
									<col style="width: 97px">
									<col style="width: 102px">
									<col style="width: 51px">
									<col style="width: 55px">
									<col style="width: 55px">
									<col style="width: 55px">
									<col style="width: 55px">
									<col style="width: 55px">
									<col style="width: 55px">
									<col style="width: 55px">
									<col style="width: 55px">
								</colgroup>
								<tr>
									<th class="tg-031e"></th>
									<th class="tg-e3zv">Naam</th>
									<th class="tg-e3zv">Voornaam</th>
									<th class="tg-e3zv">M/V</th>
									<?php for($i=1;$i<9;$i++) {?>
									<th class="tg-hgcj"><?php echo $i;?></th>
									<?php }?>
								</tr>
								<?php
									$query_student = "SELECT * FROM studenten INNER JOIN studenten_details ON studenten.studid = studenten_details.studid WHERE klasid='$klasid'";
									$result_student = mysqli_query($db, $query_student);
									$ii = 1;
									
									while($data_student = mysqli_fetch_array($result_student)) {?>
								<tr>
									<?php ${"student_".$ii} = $data_student['studdetid'];
										$_SESSION['student_'.$ii] = ${"student_".$ii};?>
									<td class="tg-s6z2"><?php echo $ii;?></td>
									<td class="tg-031e"><?php echo $data_student['naam'];?></td>
									<td class="tg-031e"><?php echo $data_student['voornaam'];?></td>
									<td class="tg-031e">M</td>
									<?php for($i=1;$i<9;$i++) {?>
									<td class="tg-031e">
										<select name="les_<?php echo $i."_".$ii;?>"
											<?php if(${"GevolgdLesuur_".$i}==2) {?> disabled <?php }?>
											>
											<?php 
												$query_presentie_status = "SELECT * FROM presentie_status";
												$result_presentie_status = mysqli_query($db, $query_presentie_status);
												while($data_presentie_status = mysqli_fetch_array($result_presentie_status)) {?>
											<option value="<?php echo $data_presentie_status['presstatid'];?>"><?php echo $data_presentie_status['status'];?></option>
											<?php }?>
										</select>
									</td>
									<?php }?>
								</tr>
								<?php
									$ii++;
									}
									?>
							</table>
						</div>
						<div style ="float:right;">
							<style type="text/css">
								.tg  {border-collapse:collapse;border-spacing:0;}
								.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg .tg-s6z2{text-align:center}
								.tg .tg-hgcj{font-weight:bold;text-align:center}
							</style>
							<table class="tg" style="undefined;table-layout: fixed; width: 364px">
								<colgroup>
									<col style="width: 61px">
									<col style="width: 239px">
									<col style="width: 64px">
								</colgroup>
								<tr>
									<th class="tg-hgcj">Lesuur</th>
									<th class="tg-hgcj">Behandelde Stof</th>
									<th class="tg-hgcj">Paraaf</th>
								</tr>
								<?php for($i=1;$i<9;$i++) {?>
								<tr>
									<td class="tg-s6z2"><?php echo $i;?></td>
									<td class="tg-031e"> <input type="text" name="behandeld_<?php echo $i;?>" style="width:100%; margin-left: -2px;" 
										<?php if(${"GevolgdLesuur_".$i}==2) {?> disabled <?php }?> required> </td>
									<td>
										<?php
											$query_docent = "SELECT * FROM docenten WHERE docid='${'doc_'.$i}'";
											$result_docent = mysqli_query($db, $query_docent);
											$data_docent = mysqli_fetch_array($result_docent);
											echo substr($data_docent['voornaam'], 0, 1).", ".substr($data_docent['naam'], 0, 4);
											//echo substr($data_docent['naam'], 0, 4);?>
									</td>
								</tr>
								<?php }?>
							</table>
						</div>
						<br><br>
						<div style="float:right;margin-top: 410px;margin-right: -365px;">
							<style type="text/css">
								.tg  {border-collapse:collapse;border-spacing:0;}
								.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg .tg-s6z2{text-align:center}
								.tg .tg-hgcj{font-weight:bold;text-align:center}
							</style>
							<table class="tg" style="undefined;table-layout: fixed; width: 314px">
								<colgroup>
									<col style="width: 61px">
									<col style="width: 303px">
								</colgroup>
								<tr>
									<th class="tg-hgcj">Lesuur</th>
									<th class="tg-hgcj">Afspraken</th>
								</tr>
								<?php for($i=1;$i<9;$i++) {?>
								<tr>
									<td class="tg-s6z2"><?php echo $i;?></td>
									<td class="tg-031e"><input type="text" name="afspraak_<?php echo $i;?>" style="width:100%; margin-left: -2px;"
										<?php if(${"GevolgdLesuur_".$i}==2) {?> disabled <?php }?>></td>
								</tr>
								<?php }?>
							</table>
						</div>
						<br/>
						<div style ="float:left; margin-top:30px;">
							<style type="text/css">
								.tg  {border-collapse:collapse;border-spacing:0;}
								.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg .tg-hgcj{font-weight:bold;text-align:center}
							</style>
							<table class="tg" style="undefined;table-layout: fixed; width: 650px">
								<colgroup>
									<col style="width: 131px">
									<col style="width: 107px">
									<col style="width: 63px">
									<col style="width: 63px">
									<col style="width: 63px">
									<col style="width: 63px">
									<col style="width: 63px">
									<col style="width: 63px">
									<col style="width: 63px">
									<col style="width: 63px">
								</colgroup>
								<tr>
									<th class="tg-031e" rowspan="2" style="text-align: left;list-style-type: none;">
										<li>G&emsp;&nbsp;= Gevolgt </li>
										<li>A&emsp;&thinsp;&thinsp;= Afwezig</li>
										<li>Z&emsp;&nbsp;&nbsp;= Ziek</li>
										<li>LT&emsp;= Laat toest</li>
										<li>LV&emsp;= Laat vertrek</li>
									</th>
									<th class="tg-hgcj">Vak</th>
									<?php for($i=1;$i<9;$i++) {?>
									<th class="tg-031e"><?php echo ${"vak_".$i};?></th>
									<?php }?>
								</tr>
								<tr>
									<td class="tg-hgcj">Paraaf</td>
									<?php for($i=1;$i<9;$i++) {?>
									<td class="tg-031e">
										<?php
											$query_docent = "SELECT * FROM docenten WHERE docid='${'doc_'.$i}'";
											$result_docent = mysqli_query($db, $query_docent);
											$data_docent = mysqli_fetch_array($result_docent);
											echo substr($data_docent['voornaam'], 0, 1).", ".substr($data_docent['naam'], 0, 4);
											//echo substr($data_docent['naam'], 0, 4);?>
									</td>
									<?php }?>
								</tr>
							</table>
							<br>
							<input type="submit" value="Opslaan" class="button" name="submitDagstaat">
					</form>
					<?php } else {?>
					<!--==============================
						Voor NEXT
						=================================-->
					<?php
					switch($_GET['dag']) {
						case 1: $dag = "Zondag"; break;
						case 2: $dag = "Maandag"; break;
						case 3: $dag = "Dinsdag"; break;
						case 4: $dag = "Woensdag"; break;
						case 5: $dag = "Donderdag"; break;
						case 6: $dag = "Vrijdag"; break;
						case 7: $dag = "Zaterdag"; break;
					}
					$_SESSION['dag'] = $dag;
					?>
					<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="GET">
					Klas: <strong><?php echo $data_klas['klas'];?></strong> &nbsp;&nbsp;&nbsp; 
					<strong>A.O</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					Week: 
					<strong>
					<select name="week" required>
					<option value=""></option> 
					<?php
						for($i=1;$i<53;$i++) {?>
					<option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php
						}
						?>
					</select>
					</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					Dag: <strong>
					<?php
					if(isset($_GET['dag'])) {
						echo $dag." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					} else {
					?>
						<select name="dag" required>
						<option value=""></option>
						<option value="Maandag">Maandag</option> 
						<option value="Dinsdag">Dinsdag</option> 
						<option value="Woensdag">Woensdag</option> 
						<option value="Donderdag">Donderdag</option> 
						<option value="Vrijdag">Vrijdag</option> 
						<option value="Zaterdag">Zaterdag</option> 
						</select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<?php
					}?>
					</strong>
					Datum: <strong>
					<?php
					if(isset($_GET['datum'])) {
						$_SESSION['datum'] = $_GET['datum'];
						echo $_GET['datum']." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
					} else { ?>
						<input type="date" name="datum" required></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					<?php
					}
					?>
					</strong>
					Naam KO: 
					<strong>
					<select name="ko" required>
					<option value=""></option> 
					<?php
						while($data_student = mysqli_fetch_array($result_student)) {?>
					<option value="<?php echo $data_student['studdetid'];?>"><?php echo $data_student['voornaam']." ".$data_student['naam'];?></option>
					<?php
						}
						?>
					</select>
					</strong> &nbsp;&nbsp;&nbsp; 
					Klassendocent: <strong><?php echo $data_docent['voornaam']." ".$data_docent['naam'];?></strong>
					<br><br>
					<style type="text/css">
					.tg  {border-collapse:collapse;border-spacing:0;}
					.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
					.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
					.tg .tg-hgcj{font-weight:bold;text-align:left; padding-left: 15px;}
					</style>
					<table class="tg" style="undefined;table-layout: fixed; width: 649px">
					<colgroup>
					<col style="width: 25px">
					<col style="width: 107px">
					<col style="width: 90px">
					<col style="width: 90px">
					<col style="width: 90px">
					<col style="width: 90px">
					<col style="width: 90px">
					<col style="width: 90px">
					<col style="width: 90px">
					<col style="width: 90px">
					</colgroup>
					<tr>
					<th class="tg-hgcj" colspan="2">Vak</th>
					<?php for($i=1;$i<9;$i++) {?>
					<th class="tg-031e"><input type="text" placeholder="Vak" name="vak_<?php echo $i;?>" style="width:100%; margin-left: -2px;" required></th>
					<?php }?>
					</tr>
					<tr>
					<td class="tg-hgcj" colspan="2">Docent</td>
					<?php for($i=1;$i<9;$i++) {?>
					<td class="tg-031e">
					<select name="doc_<?php echo $i;?>" style="width:100%; margin-left: -2px;" required>
					<option value=""></option> 
					<?php
						$query_docent = "SELECT * FROM docenten";
						$result_docent = mysqli_query($db, $query_docent);
						while($data_docent = mysqli_fetch_array($result_docent)) {?>
					<option value="<?php echo $data_docent['docid'];?>"><?php echo $data_docent['naam'];?></option>
					<?php }?>
					</select>
					</td>
					<?php }?>
					</tr>
					<tr>
					<td class="tg-hgcj" colspan="2">Niet Gevolgd</td>
					<?php for($i=1;$i<9;$i++) {?>
					<td class="tg-031e"><!--<input type="checkbox" name="GevolgdLesuur<?php //echo $i;?>" value="false" >Lesuur <?php //echo $i;?>-->
					<select name="GevolgdLesuur<?php echo $i;?>" style="width:100%; margin-left: -2px;">
					<?php
						$query_les_status = "SELECT * FROM les_status";
						$result_les_status = mysqli_query($db, $query_les_status);
						while($data_les_status = mysqli_fetch_array($result_les_status)) {?>
					<option value="<?php echo $data_les_status['lesstid'];?>"><?php echo $data_les_status['status'];?></option>
					<?php }?>
					</select>
					</td>
					<?php }?>
					</tr>
					</table>
					<br>
					<input type="submit" value="Next" class ="button" name="next">
					</form>
					<?php }?>
					</div>
				</div>
				</div>
			</center>
		</section>
		<!--==============================
			footer
			=================================-->
		<?php include ("inc/footer.php"); ?>
	</body>
</html>