<?php session_start();
	include ("inc/db.php");
	
	if(!isset($_SESSION['gebruikersnaam'])) {
		header('Location: index.php');
	}
	if(isset($_POST['opermking_submit'])) {
		$studdetid = $_POST['student'];
		$docid = $_POST['docent'];
		$datum = $_POST['datum'];
		$opmerking = $_POST['opmerking'];
		
		$query_insert_memo = "INSERT INTO `memo_opmerkingen` (`studdetid`, `docentid`, `datum`, `opmerking`) 
								VALUES ('$studdetid', '$docid', '$datum', '$opmerking')";
		$result_insert_memo = mysqli_query($db, $query_insert_memo);
		
		if(mysqli_affected_rows($db) == 1) {
			mysqli_close($db);
			
			header('Location: memo_opmerking.php?studdetid='.$studdetid.'&type=0');
		}
	} else if(!isset($_GET['studdetid'])) {
		header('Location: studenten.php');
	}?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>LVS | MEMO</title>
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no">
<link rel="icon" href="images/favicon.ico">
<link rel="shortcut icon" href="images/favicon.ico">
<link rel="stylesheet" href="css/stuck.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/ihover.css">
<script src="js/jquery.js"></script>
<script src="js/jquery-migrate-1.1.1.js"></script>
<script src="js/script.js"></script>
<script src="js/superfish.js"></script>
<script src="js/jquery.equalheights.js"></script>
<script src="js/jquery.mobilemenu.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/tmStickUp.js"></script>
<script src="js/jquery.ui.totop.js"></script>
<script>
 $(document).ready(function(){
  $().UItoTop({ easingType: 'easeOutQuart' });
  $('#stuck_container').tmStickUp({});
  });
</script>
<!--[if lt IE 9]>
 <div style=' clear: both; text-align:center; position: relative;'>
   <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
     <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
   </a>
</div>
<script src="js/html5shiv.js"></script>
<link rel="stylesheet" media="screen" href="css/ie.css">
<![endif]-->
<!--[if lt IE 10]>
<link rel="stylesheet" media="screen" href="css/ie1.css">
<![endif]-->
<!-- Print Option -->
<script>
	function printContent(printPage){
		var restorepage = document.body.innerHTML;
		var print = document.getElementById(printPage).innerHTML;
		document.body.innerHTML = print;
		window.print();
		document.body.innerHTML = restorepage;
	}
</script>

</head>
<body class="page1" id="top">
<!--==============================
              header
=================================-->
<header>
<!--==============================
            Stuck menu
=================================-->
<?php include ("inc/header.php"); ?>
</header>
<!--=====================
          Content
======================-->
<section class="content"><div class="ic">More Website Templates @ TemplateMonster.com - June 16, 2014!</div>
			<button onclick="printContent('printPage')" style="width: 57px; height: 57px; margin-top: 59px;/* padding-left: 20px; *//* padding-right: 20px; */padding-top: 5px;padding-bottom: 5px;background: rgb(16, 70, 16); color: white;border: 0px;position:fixed;text-align: center;"><div style="margin-top:2px;">Print </div><img src="images/print.png"></img></button>
			<div id="printPage" class="container">
  <div style= "width: 850px; margin-left:auto; margin-right:auto;">
  <?php
  	$studdetid = $_GET['studdetid'];
	$query_student = "SELECT * FROM studenten INNER JOIN studenten_details ON studenten.studid = studenten_details.studid INNER JOIN klassen ON studenten_details.klasid = klassen.klasid INNER JOIN schooljaar ON klassen.sjid = schooljaar.sjid INNER JOIN geslacht ON studenten.geslachtid = geslacht.geslachtid WHERE studdetid='$studdetid'";
	$result_student = mysqli_query($db, $query_student);
	$data_student = mysqli_fetch_array($result_student);
  
  if($_GET['type']==1) { ?>
  	  <p><h1 style="font-size: 30px; text-align:center;"><a href="studentenkaart.php?studdetid=<?= $studdetid;?>"><?= $data_student['voornaam']." ".$data_student['naam'];?></a> - Memo Invullen</h1></p>

	<style type="text/css">
	.tg  {border-collapse:collapse;border-spacing:0;}
	.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
	.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
	.tg .tg-uyc4{font-weight:bold;background-color:#104610;color:#ffffff}
	.tg .tg-cxkv{background-color:#ffffff}
	</style>
	<center>
	<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
	<table class="tg" style="undefined;table-layout: fixed; width: 500px">
	<colgroup>
	<col style="width: 130px">
	<col style="width: 21px">
	<col style="width: 49px">
	<col style="width: 49px">
	<col style="width: 112px">
	</colgroup>
	  <tr>
	    <th class="tg-uyc4" colspan="2">Student Naam</th>
	    <th class="tg-cxkv" colspan="3"><input type="text" value="<?php echo $data_student['voornaam']." ".$data_student['naam']; ?>" style="width:100%; margin-left:-2px;" disabled=""/></th>
	  </tr>
	  <tr>
	    <th class="tg-uyc4" colspan="2">Docent Naam</th>
	    <th class="tg-cxkv" colspan="3"><input type="text" value="<?php echo $_SESSION['volNaam']; ?>" style="width:100%; margin-left:-2px;" disabled=""/></th>
	  </tr>
	  <tr>
	    <td class="tg-uyc4" colspan="2">Datum</td>
	    <td class="tg-cxkv" colspan="3"><input type="date" name="datum" style="width:100%; margin-left:-2px;" /></td>
	  </tr>
	  <tr>
	    <td class="tg-uyc4" colspan="2">Opmerking</td>
	    <td class="tg-cxkv" colspan="3">
	    	<textarea name="opmerking"></textarea>
	    	<input name="student" value="<?php echo $data_student['studdetid'];?>" hidden />
	    	<input name="docent" value="<?php echo $_SESSION['gebruikersid'];?>" hidden />
	    </td>
	  </tr>
	  <tr>
	  	<td colspan="5"><input type="submit" name="opermking_submit" value="Opmerking Opslaan" /></td>
	  </tr>
	</table>
	</form>
	</center>
  <?php
  } else if($_GET['type']==0) { ?>
  	<p><h1 style="font-size: 30px; text-align:center;"><a href="studentenkaart.php?studdetid=<?= $studdetid;?>"><?= $data_student['voornaam']." ".$data_student['naam'];?></a> - Memo Bekijken</h1></p>

	<center>
	<style type="text/css">
	.tg  {border-collapse:collapse;border-spacing:0;}
	.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
	.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
	.tg .tg-cxkv{background-color:#ffffff}
	.tg .tg-2ro4{font-weight:bold;background-color:#ffffff;color:#000000}
	.tg .tg-ju3u{font-weight:bold;background-color:#104610;color:#ffffff;text-align:center}
	</style>
	<table class="tg" style="undefined;table-layout: fixed; width: 813px">
	    <th class="tg-ju3u">Aantal</th>
	    <th class="tg-ju3u">Datum</th>
	    <th class="tg-ju3u">Docent</th>
	    <th class="tg-ju3u">Opmerking</th>
	  </tr>
	  <?php
	  $studdetid = $data_student['studdetid'];
	  $query_memo= "SELECT * FROM memo_opmerkingen INNER JOIN docenten ON memo_opmerkingen.docentid=docenten.docid WHERE studdetid='$studdetid'";
	  $result_memo = mysqli_query($db, $query_memo);
	  $i = 1;
									
	  while($data_memo = mysqli_fetch_array($result_memo)) { ?>
	  	<tr>
	      <td class="tg-031e"><?php echo $i;?></td>
	      <td class="tg-031e"><?php echo $data_memo['datum'];?></td>
	      <td class="tg-031e"><?php echo $data_memo['voornaam']." ".$data_memo['naam'];?></td>
	      <td class="tg-031e"><?php echo $data_memo['opmerking'];?></td>
	    </tr>
	  <?php
	  	$i++;
	  }
	  ?>
	</table>
	
	</center>	
  <?php
  }
  ?>	
</div>	
	
  </div>
</section>
<!--==============================
              footer
=================================-->
<?php include ("inc/footer.php"); ?>
</body>
</html>

