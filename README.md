# README #

### What is this repository for? ###

This was a school project from when I was in Institute for Natural Resource and Engineering Studies (NATIN).
It is a student management system.

### How do I get set up? ###

* Pull files
* Put files on a LAMP server
* In the db folder you will find a sql file
* Import the file into a database
* Change db details in inc/db.php


### This project is in Dutch ###