-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 13, 2015 at 02:15 AM
-- Server version: 5.6.17
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `natinlvs`
--
CREATE DATABASE IF NOT EXISTS `natinlvs` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `natinlvs`;

-- --------------------------------------------------------

--
-- Table structure for table `afspraken`
--

CREATE TABLE IF NOT EXISTS `afspraken` (
  `afsprakenid` int(11) NOT NULL AUTO_INCREMENT,
  `presid` int(11) NOT NULL,
  `afspraak_1` longtext,
  `afspraak_2` longtext,
  `afspraak_3` longtext,
  `afspraak_4` longtext,
  `afspraak_5` longtext,
  `afspraak_6` longtext,
  `afspraak_7` longtext,
  `afspraak_8` longtext,
  PRIMARY KEY (`afsprakenid`),
  KEY `afspr_pres_idx` (`presid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `afspraken`
--

INSERT INTO `afspraken` (`afsprakenid`, `presid`, `afspraak_1`, `afspraak_2`, `afspraak_3`, `afspraak_4`, `afspraak_5`, `afspraak_6`, `afspraak_7`, `afspraak_8`) VALUES
(21, 21, '', '', '', '', '', '', '', ''),
(23, 23, '', '', '', '', '', '', '', ''),
(24, 24, '', '', '', '', '', '', '', ''),
(25, 25, '', '', '', '', '', '', '', ''),
(26, 26, '', '', '', '', '', '', '', ''),
(27, 27, '', '', 'De leerlingen waren lui', '', '', '', '', ''),
(28, 28, '', '', '', '', '', '', '', ''),
(29, 29, '', '', '', '', '', '', '', ''),
(30, 30, '', '', '', '', '', '', '', ''),
(31, 31, '', '', '', '', '', '', '', ''),
(32, 32, '', '', '', '', '', '', '', ''),
(33, 33, '', '', '', '', '', '', '', ''),
(34, 34, 'test', '', '', '', '', '', '', ''),
(35, 35, 'NIET VERPLICHT', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `behandeld`
--

CREATE TABLE IF NOT EXISTS `behandeld` (
  `behandid` int(11) NOT NULL AUTO_INCREMENT,
  `presid` int(11) NOT NULL,
  `behandeld_1` longtext,
  `behandeld_2` longtext,
  `behandeld_3` longtext,
  `behandeld_4` longtext,
  `behandeld_5` longtext,
  `behandeld_6` longtext,
  `behandeld_7` longtext,
  `behandeld_8` longtext,
  PRIMARY KEY (`behandid`),
  KEY `behand_pres_idx` (`presid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `behandeld`
--

INSERT INTO `behandeld` (`behandid`, `presid`, `behandeld_1`, `behandeld_2`, `behandeld_3`, `behandeld_4`, `behandeld_5`, `behandeld_6`, `behandeld_7`, `behandeld_8`) VALUES
(21, 21, 'Sprint', 'Sprint', 'Functies', 'Functies', 'LVS', 'LVS', '', ''),
(23, 23, 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'TEST'),
(24, 24, 'LVS', 'LVS', 'LVS', 'LVS', 'LVS', 'APP', 'NIETS', 'NIETS'),
(25, 25, 'test', 'test', '', '', 'test', 'test', 'test', 'test'),
(26, 26, 'test', 'test', 'test', 'test', 'test', 'test', '', ''),
(27, 27, '', '', 'test', 'test', 'test', 'test', 'test', 'test'),
(28, 28, 'test', 'test', 'test', 'test', 'TEST', 'test', 'test', 'test'),
(29, 29, 'jup', 'test', '', '', 'again', 'like', 'about', 'a week ago'),
(30, 30, 'fbn', 'fh', 'fn', 'fdn', 'ffbn', 'fdn', 'fn', 'fdnb'),
(31, 31, '', '', 'Portfolio', 'Portfolio', 'LVS en Drupal', 'LVS en Drupal', '', ''),
(32, 32, '', 'ok', 'ok', 'ok', 'ok', 'ok', '', ''),
(33, 33, '', '', 'nee', 'nee', 'nee', 'nee', '', ''),
(34, 34, 'behandeld', 'behandeld', 'behandeld', 'behandeld', '', '', 'behandeld', 'behandeld'),
(35, 35, 'PRESENTATIE', 'PRESENTATIE', '', '', 'PRESENTATIE', 'PRESENTATIE', 'PRESENTATIE', 'PRESENTATIE');

-- --------------------------------------------------------

--
-- Table structure for table `bevoegdheden`
--

CREATE TABLE IF NOT EXISTS `bevoegdheden` (
  `bevid` int(11) NOT NULL AUTO_INCREMENT,
  `bevoegdheid` varchar(45) NOT NULL,
  PRIMARY KEY (`bevid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bevoegdheden`
--

INSERT INTO `bevoegdheden` (`bevid`, `bevoegdheid`) VALUES
(1, 'Directeur'),
(2, 'Onder Directeur'),
(3, 'Docent'),
(4, 'Richtings Coördinator');

-- --------------------------------------------------------

--
-- Table structure for table `docenten`
--

CREATE TABLE IF NOT EXISTS `docenten` (
  `docid` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(45) NOT NULL,
  `voornaam` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `geslid` int(11) NOT NULL,
  PRIMARY KEY (`docid`),
  KEY `doc_geslacht_idx` (`geslid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `docenten`
--

INSERT INTO `docenten` (`docid`, `naam`, `voornaam`, `email`, `geslid`) VALUES
(1, 'Raghosing', 'Sharda', 'sharda@natin-lvs.com', 2),
(2, 'Vliese', 'Abigail', 'vliese@natin-lvs.com', 2),
(3, 'Schutte', 'Sharida', 'schutte@natin-lvs.com', 2),
(4, 'Groenberg', 'Rochelle', 'groenberg@natin-lvs.com', 2),
(5, 'Ramrattan', 'Wicky', 'ramrattan@natin-lvs.com', 1),
(6, 'Sewradj', 'Roy', 'sewradj@natin-lvs.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `geslacht`
--

CREATE TABLE IF NOT EXISTS `geslacht` (
  `geslachtid` int(11) NOT NULL AUTO_INCREMENT,
  `geslacht` varchar(45) NOT NULL,
  PRIMARY KEY (`geslachtid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `geslacht`
--

INSERT INTO `geslacht` (`geslachtid`, `geslacht`) VALUES
(1, 'Man'),
(2, 'Vrouw');

-- --------------------------------------------------------

--
-- Table structure for table `klassen`
--

CREATE TABLE IF NOT EXISTS `klassen` (
  `klasid` int(11) NOT NULL AUTO_INCREMENT,
  `sjid` int(11) NOT NULL,
  `ljid` int(11) NOT NULL,
  `docid` int(11) NOT NULL,
  `klas` varchar(45) NOT NULL,
  `secid` int(11) NOT NULL,
  PRIMARY KEY (`klasid`),
  KEY `klas_sj_idx` (`sjid`),
  KEY `klas_leerj_idx` (`ljid`),
  KEY `klas_doc_idx` (`docid`),
  KEY `klas_sect_idx` (`secid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `klassen`
--

INSERT INTO `klassen` (`klasid`, `sjid`, `ljid`, `docid`, `klas`, `secid`) VALUES
(1, 4, 3, 1, '3.10.2', 4),
(2, 5, 4, 1, '4.10.2', 4),
(3, 5, 3, 2, '3.10.1', 4),
(4, 5, 4, 3, '4.10.1', 3);

-- --------------------------------------------------------

--
-- Table structure for table `leerjaren`
--

CREATE TABLE IF NOT EXISTS `leerjaren` (
  `ljid` int(11) NOT NULL AUTO_INCREMENT,
  `leerjaar` varchar(45) NOT NULL,
  PRIMARY KEY (`ljid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `leerjaren`
--

INSERT INTO `leerjaren` (`ljid`, `leerjaar`) VALUES
(1, '1'),
(2, '2'),
(3, '3'),
(4, '4');

-- --------------------------------------------------------

--
-- Table structure for table `les_status`
--

CREATE TABLE IF NOT EXISTS `les_status` (
  `lesstid` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`lesstid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `les_status`
--

INSERT INTO `les_status` (`lesstid`, `status`) VALUES
(1, 'Verzorgd'),
(2, 'Niet verzorgd');

-- --------------------------------------------------------

--
-- Table structure for table `logindata`
--

CREATE TABLE IF NOT EXISTS `logindata` (
  `logid` int(11) NOT NULL AUTO_INCREMENT,
  `bevid` int(11) NOT NULL,
  `docid` int(11) NOT NULL,
  `gebruikersnaam` varchar(45) NOT NULL,
  `wachtwoord` varchar(45) NOT NULL,
  PRIMARY KEY (`logid`),
  KEY `login_bev_idx` (`bevid`),
  KEY `login_doc_idx` (`docid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `logindata`
--

INSERT INTO `logindata` (`logid`, `bevid`, `docid`, `gebruikersnaam`, `wachtwoord`) VALUES
(1, 3, 1, 'sraghosing', 'adminroot'),
(2, 3, 2, 'vliese', 'vliese'),
(3, 3, 3, 'schutte', 'schutte'),
(4, 1, 4, 'groenberg', 'groenberg'),
(5, 4, 5, 'ramrattan', 'ramrattan'),
(6, 4, 6, 'sewradj', 'sewradj');

-- --------------------------------------------------------

--
-- Table structure for table `memo_opmerkingen`
--

CREATE TABLE IF NOT EXISTS `memo_opmerkingen` (
  `idmemo_opmerkingen` int(11) NOT NULL AUTO_INCREMENT,
  `studdetid` int(11) NOT NULL,
  `docentid` int(11) NOT NULL,
  `datum` varchar(45) NOT NULL,
  `opmerking` longtext,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmemo_opmerkingen`),
  KEY `studenten_idx` (`studdetid`),
  KEY `docenten_idx` (`docentid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `memo_opmerkingen`
--

INSERT INTO `memo_opmerkingen` (`idmemo_opmerkingen`, `studdetid`, `docentid`, `datum`, `opmerking`, `timestamp`) VALUES
(1, 4, 1, '2015-02-03', 'Hij is weg geboort', '2015-02-04 03:57:33'),
(2, 1, 1, '2015-02-02', 'Let niet op in de klas', '2015-02-04 03:59:03'),
(3, 3, 1, '2015-02-02', 'hfhnfgn', '2015-02-04 04:00:56'),
(4, 7, 1, '2015-02-04', 'fnfgnf', '2015-02-04 04:01:43'),
(5, 2, 1, '2015-02-03', 'dbdvb', '2015-02-04 04:02:20'),
(6, 6, 1, '2015-02-02', 'bvdbd', '2015-02-04 04:03:00'),
(7, 5, 1, '2015-02-02', 'Is altijd optijd', '2015-02-04 04:03:46'),
(8, 5, 1, '2015-02-04', 'gbnf', '2015-02-04 04:04:33'),
(9, 1, 1, '2015-02-02', 'redtyjlo;k', '2015-02-04 12:01:30');

-- --------------------------------------------------------

--
-- Table structure for table `niet_verzorgd`
--

CREATE TABLE IF NOT EXISTS `niet_verzorgd` (
  `idniet_verzorgd` int(11) NOT NULL AUTO_INCREMENT,
  `presid` int(11) NOT NULL,
  `lesuur` varchar(45) NOT NULL,
  `reden` mediumtext,
  `inhaal` varchar(45) DEFAULT NULL,
  `afgehandeld` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idniet_verzorgd`),
  KEY `presid_idx` (`presid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `niet_verzorgd`
--

INSERT INTO `niet_verzorgd` (`idniet_verzorgd`, `presid`, `lesuur`, `reden`, `inhaal`, `afgehandeld`) VALUES
(1, 21, '7', 'Ik had geen zin', '2014-11-21', '2014-11-21'),
(2, 21, '8', 'Ben vergeten', '2014-11-24', ''),
(3, 25, '3', 'Zoo.......', '2014-11-24', ''),
(4, 25, '4', 'ik ook niet', '2014-12-03', '2014-12-18'),
(5, 26, '7', NULL, NULL, NULL),
(6, 26, '8', NULL, NULL, NULL),
(7, 27, '1', NULL, NULL, NULL),
(8, 27, '2', NULL, NULL, NULL),
(9, 31, '1', NULL, NULL, NULL),
(10, 31, '2', NULL, NULL, NULL),
(11, 31, '7', NULL, NULL, NULL),
(12, 31, '8', 'We zien nog', '2014-11-21', ''),
(13, 29, '3', 'Even nadenken', '2014-12-01', ''),
(14, 29, '4', 'Nou ja, nog een keer??', '2014-12-08', ''),
(15, 32, '1', 'Ben vergeten', '2014-11-27', ''),
(16, 32, '7', NULL, NULL, NULL),
(17, 32, '8', NULL, NULL, NULL),
(18, 33, '1', 'klaar met hun project', '', ''),
(19, 33, '2', NULL, NULL, NULL),
(20, 33, '7', NULL, NULL, NULL),
(21, 33, '8', NULL, NULL, NULL),
(22, 34, '5', NULL, NULL, NULL),
(23, 34, '6', NULL, NULL, NULL),
(24, 35, '3', 'PRESENTATIE', '2014-12-19', '2014-12-15'),
(25, 35, '4', 'PRESENTATIE', '2014-12-12', '2014-12-24');

-- --------------------------------------------------------

--
-- Table structure for table `opmerkingen`
--

CREATE TABLE IF NOT EXISTS `opmerkingen` (
  `opmid` int(11) NOT NULL AUTO_INCREMENT,
  `klasid` int(11) NOT NULL,
  `week` int(11) NOT NULL,
  `dag` varchar(45) NOT NULL,
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `docid` int(11) NOT NULL,
  `studid` int(11) NOT NULL,
  `opmerking` longtext NOT NULL,
  PRIMARY KEY (`opmid`),
  KEY `opm_klas_idx` (`klasid`),
  KEY `opm_stud_idx` (`studid`),
  KEY `opm_doc_idx` (`docid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `presentie`
--

CREATE TABLE IF NOT EXISTS `presentie` (
  `presid` int(11) NOT NULL AUTO_INCREMENT,
  `klasid` int(11) NOT NULL,
  `week` int(11) NOT NULL,
  `dag` varchar(45) NOT NULL,
  `datum` varchar(45) NOT NULL,
  `studid` int(11) NOT NULL,
  `docid_1` int(11) NOT NULL,
  `les_1` int(11) NOT NULL,
  `vak_1` varchar(45) NOT NULL,
  `docid_2` int(11) NOT NULL,
  `les_2` int(11) NOT NULL,
  `vak_2` varchar(45) NOT NULL,
  `docid_3` int(11) NOT NULL,
  `les_3` int(11) NOT NULL,
  `vak_3` varchar(45) NOT NULL,
  `docid_4` int(11) NOT NULL,
  `les_4` int(11) NOT NULL,
  `vak_4` varchar(45) NOT NULL,
  `docid_5` int(11) NOT NULL,
  `les_5` int(11) NOT NULL,
  `vak_5` varchar(45) NOT NULL,
  `docid_6` int(11) NOT NULL,
  `les_6` int(11) NOT NULL,
  `vak_6` varchar(45) NOT NULL,
  `docid_7` int(11) NOT NULL,
  `les_7` int(11) NOT NULL,
  `vak_7` varchar(45) NOT NULL,
  `docid_8` int(11) NOT NULL,
  `les_8` int(11) NOT NULL,
  `vak_8` varchar(45) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`presid`),
  KEY `les1_status_idx` (`les_1`),
  KEY `les2_status_idx` (`les_2`),
  KEY `les3_status_idx` (`les_3`),
  KEY `les4_status_idx` (`les_4`),
  KEY `les5_status_idx` (`les_5`),
  KEY `les6_status_idx` (`les_6`),
  KEY `les7_status_idx` (`les_7`),
  KEY `les8_status_idx` (`les_8`),
  KEY `pres_klas_idx` (`klasid`),
  KEY `pres_stud_idx` (`studid`),
  KEY `pres_doc_idx` (`docid_1`),
  KEY `pres_doc2_idx` (`docid_2`),
  KEY `pres_doc3_idx` (`docid_3`),
  KEY `pres_doc4_idx` (`docid_4`),
  KEY `pres_doc5_idx` (`docid_5`),
  KEY `pres_doc6_idx` (`docid_6`),
  KEY `pres_doc7_idx` (`docid_7`),
  KEY `pres_doc8_idx` (`docid_8`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `presentie`
--

INSERT INTO `presentie` (`presid`, `klasid`, `week`, `dag`, `datum`, `studid`, `docid_1`, `les_1`, `vak_1`, `docid_2`, `les_2`, `vak_2`, `docid_3`, `les_3`, `vak_3`, `docid_4`, `les_4`, `vak_4`, `docid_5`, `les_5`, `vak_5`, `docid_6`, `les_6`, `vak_6`, `docid_7`, `les_7`, `vak_7`, `docid_8`, `les_8`, `vak_8`, `timestamp`) VALUES
(21, 1, 2, 'Maandag', '2014-11-21', 4, 4, 1, 'ENG', 4, 1, 'ENG', 3, 1, 'NED', 3, 1, 'NED', 1, 1, 'WIS', 1, 1, 'WIS', 6, 2, 'DIST', 6, 2, 'DIST', '2014-11-08 03:50:54'),
(23, 1, 3, 'Woensdag', '2014-11-02', 4, 5, 1, 'GYM', 5, 1, 'GYM', 2, 1, 'WIS', 2, 1, 'WIS', 1, 1, 'DIST', 1, 1, 'DIST', 4, 1, 'MAD', 4, 1, 'MAD', '2014-11-08 03:50:50'),
(24, 1, 35, 'Maandag', '2014-10-06', 4, 1, 1, 'DIST', 1, 1, 'DIST', 1, 1, 'PROJ', 1, 1, 'PROJ', 5, 1, 'MAD', 5, 1, 'MAD', 5, 1, 'MAD', 5, 1, 'MAD', '2014-10-06 01:03:14'),
(25, 1, 23, 'Donderdag', '2014-10-16', 2, 1, 1, 'Security', 1, 1, 'Security', 2, 2, 'LB', 2, 2, 'LB', 1, 1, 'PROJ', 1, 1, 'PROJ', 1, 1, 'PROJ', 1, 1, 'PROJ', '2014-10-16 11:30:36'),
(26, 1, 13, 'Woensdag', '2014-10-15', 2, 2, 1, 'ENG', 2, 1, 'ENG', 1, 1, 'NED', 1, 1, 'NED', 3, 1, 'WIS', 3, 1, 'WIS', 5, 2, 'MAD', 5, 2, 'MAD', '2014-10-16 13:44:46'),
(27, 1, 35, 'Vrijdag', '2014-11-17', 2, 2, 2, 'NED', 2, 2, 'NED', 1, 1, 'PROJ', 1, 1, 'PROJ', 3, 1, 'PROJ', 3, 1, 'PROJ', 5, 1, 'PROJ', 5, 1, 'PROJ', '2014-11-08 04:22:04'),
(28, 2, 5, 'Maandag', '2014-11-08', 7, 4, 1, 'ghj', 3, 1, 'fhj', 3, 1, 'fhj', 2, 1, 'ghj', 2, 1, 'gj', 2, 1, 'gj', 1, 1, 'gj', 1, 1, 'gj', '2014-11-08 04:16:51'),
(29, 2, 4, 'Donderdag', '2014-11-07', 5, 2, 1, 'gm', 3, 1, 'ghm', 3, 2, 'ghm', 3, 2, 'fghm', 2, 1, 'gfhmgf', 2, 1, 'hmfg', 2, 1, 'hmgf', 2, 1, 'hgj', '2014-11-08 18:38:17'),
(30, 3, 44, 'Woensdag', '2014-11-19', 8, 1, 1, 'fbn', 2, 1, 'fhn', 2, 1, 'fn', 4, 1, 'fn', 2, 1, 'fn', 3, 1, 'fn', 3, 1, 'fn', 4, 1, 'fn', '2014-11-19 02:49:18'),
(31, 1, 46, 'Donderdag', '2014-11-20', 2, 1, 2, 'DIST', 1, 2, 'DIST', 2, 1, 'LB', 2, 1, 'LB', 1, 1, 'PROJ', 1, 1, 'PROJ', 3, 2, 'PROJ', 2, 2, 'PROJ', '2014-11-20 00:22:28'),
(32, 2, 47, 'Vrijdag', '2014-11-21', 5, 2, 2, 'ENG', 2, 1, 'ENG', 3, 1, 'NED', 3, 1, 'NED', 5, 1, 'CLOUD', 5, 1, 'CLOUD', 5, 2, 'SEC', 5, 2, 'SEC', '2014-11-21 11:40:06'),
(33, 3, 47, 'Vrijdag', '2014-11-21', 8, 5, 2, 'CLOUD', 5, 2, 'CLOUD', 5, 1, 'VIRT', 5, 1, 'VIRT', 3, 1, 'NED', 3, 1, 'NED', 2, 2, 'ENG', 2, 2, 'ENG', '2014-11-21 11:51:31'),
(34, 1, 48, 'Woensdag', '2014-11-26', 2, 1, 1, 'DIST', 1, 1, 'DIST', 2, 1, 'NED', 2, 1, 'NED', 3, 2, 'WISK', 3, 2, 'WISK', 5, 1, 'PROJ', 5, 1, 'PROJ', '2014-11-26 11:46:34'),
(35, 1, 18, 'Donderdag', '2014-12-18', 2, 1, 1, 'NED', 1, 1, 'NED', 2, 2, 'WIS', 2, 2, 'WIS', 3, 1, 'ENG', 3, 1, 'ENG', 5, 1, 'GYM', 5, 1, 'GYM', '2014-12-18 15:20:13');

-- --------------------------------------------------------

--
-- Table structure for table `presentie_details`
--

CREATE TABLE IF NOT EXISTS `presentie_details` (
  `presdetid` int(11) NOT NULL AUTO_INCREMENT,
  `presid` int(11) NOT NULL,
  `studid` int(11) NOT NULL,
  `les_1` int(11) DEFAULT NULL,
  `les_2` int(11) DEFAULT NULL,
  `les_3` int(11) DEFAULT NULL,
  `les_4` int(11) DEFAULT NULL,
  `les_5` int(11) DEFAULT NULL,
  `les_6` int(11) DEFAULT NULL,
  `les_7` int(11) DEFAULT NULL,
  `les_8` int(11) DEFAULT NULL,
  PRIMARY KEY (`presdetid`),
  KEY `pres_presstat_idx` (`presid`),
  KEY `presdet_stud_idx` (`studid`),
  KEY `pres1_status_idx` (`les_1`),
  KEY `pres2_status_idx` (`les_2`),
  KEY `pres3_status_idx` (`les_3`),
  KEY `pres4_status_idx` (`les_4`),
  KEY `pres5_status_idx` (`les_5`),
  KEY `pres6_status_idx` (`les_6`),
  KEY `pres7_status_idx` (`les_7`),
  KEY `pres8_status_idx` (`les_8`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=110 ;

--
-- Dumping data for table `presentie_details`
--

INSERT INTO `presentie_details` (`presdetid`, `presid`, `studid`, `les_1`, `les_2`, `les_3`, `les_4`, `les_5`, `les_6`, `les_7`, `les_8`) VALUES
(55, 21, 1, 4, 1, 1, 1, 4, 1, 2, 2),
(56, 21, 2, 1, 1, 1, 1, 1, 1, 2, 2),
(57, 21, 3, 1, 1, 1, 1, 1, 1, 2, 2),
(58, 21, 4, 1, 1, 1, 1, 1, 1, 2, 2),
(63, 23, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(64, 23, 2, 2, 2, 2, 2, 2, 2, 2, 2),
(65, 23, 3, 1, 1, 1, 1, 1, 1, 1, 1),
(66, 23, 4, 1, 1, 1, 1, 1, 1, 1, 1),
(67, 24, 1, 4, 1, 1, 1, 1, 1, 2, 1),
(68, 24, 2, 4, 1, 1, 1, 1, 1, 2, 2),
(69, 24, 3, 4, 1, 1, 1, 1, 1, 2, 2),
(70, 24, 4, 4, 1, 1, 1, 1, 1, 2, 2),
(71, 25, 1, 1, 1, 2, 2, 1, 1, 1, 1),
(72, 25, 2, 1, 1, 2, 2, 1, 1, 1, 1),
(73, 25, 3, 1, 1, 2, 2, 1, 1, 1, 1),
(74, 25, 4, 1, 1, 2, 2, 1, 1, 1, 1),
(75, 26, 1, 4, 1, 1, 1, 1, 1, 2, 2),
(76, 26, 2, 4, 1, 1, 1, 1, 1, 2, 2),
(77, 26, 3, 4, 1, 1, 1, 1, 1, 2, 2),
(78, 26, 4, 4, 1, 1, 1, 1, 1, 2, 2),
(79, 27, 1, 2, 2, 4, 1, 1, 1, 1, 1),
(80, 27, 2, 2, 2, 4, 1, 1, 1, 1, 1),
(81, 27, 3, 2, 2, 4, 1, 1, 1, 1, 1),
(82, 27, 4, 2, 2, 4, 1, 1, 1, 1, 1),
(83, 28, 5, 4, 1, 1, 1, 1, 1, 2, 2),
(84, 28, 6, 4, 1, 1, 1, 1, 1, 2, 2),
(85, 28, 7, 4, 1, 1, 1, 1, 1, 2, 2),
(86, 29, 5, 4, 1, 2, 2, 1, 1, 1, 1),
(87, 29, 6, 4, 1, 2, 2, 1, 1, 1, 1),
(88, 29, 7, 4, 1, 2, 2, 1, 1, 1, 1),
(89, 30, 8, 1, 1, 1, 1, 1, 1, 4, 1),
(90, 30, 9, 1, 1, 1, 1, 1, 1, 4, 1),
(91, 30, 10, 1, 1, 1, 1, 1, 1, 4, 1),
(92, 31, 1, 2, 2, 1, 1, 1, 1, 2, 2),
(93, 31, 2, 2, 2, 1, 1, 1, 1, 2, 2),
(94, 31, 3, 2, 2, 1, 1, 1, 1, 2, 2),
(95, 31, 4, 2, 2, 1, 1, 1, 1, 2, 2),
(96, 32, 5, 2, 4, 1, 1, 1, 1, 2, 2),
(97, 32, 6, 2, 4, 1, 1, 1, 1, 2, 2),
(98, 32, 7, 2, 2, 1, 1, 1, 1, 2, 2),
(99, 33, 8, 2, 2, 1, 1, 1, 1, 2, 2),
(100, 33, 9, 2, 2, 1, 1, 1, 1, 2, 2),
(101, 33, 10, 2, 2, 1, 1, 1, 5, 2, 2),
(102, 34, 1, 4, 1, 1, 1, 2, 2, 1, 1),
(103, 34, 2, 1, 1, 1, 1, 2, 2, 1, 1),
(104, 34, 3, 1, 1, 1, 1, 2, 2, 1, 1),
(105, 34, 4, 1, 1, 1, 1, 2, 2, 2, 2),
(106, 35, 1, 1, 1, 2, 2, 1, 1, 1, 1),
(107, 35, 2, 1, 1, 2, 2, 1, 1, 1, 1),
(108, 35, 3, 1, 1, 2, 2, 1, 1, 1, 1),
(109, 35, 4, 4, 4, 2, 2, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `presentie_status`
--

CREATE TABLE IF NOT EXISTS `presentie_status` (
  `presstatid` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`presstatid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `presentie_status`
--

INSERT INTO `presentie_status` (`presstatid`, `status`) VALUES
(1, 'G'),
(2, 'A'),
(3, 'Z'),
(4, 'Lt'),
(5, 'Lv');

-- --------------------------------------------------------

--
-- Table structure for table `richtings_coordinator`
--

CREATE TABLE IF NOT EXISTS `richtings_coordinator` (
  `rcid` int(11) NOT NULL AUTO_INCREMENT,
  `docid` int(11) NOT NULL,
  `secid` int(11) NOT NULL,
  PRIMARY KEY (`rcid`),
  KEY `rc_doc_idx` (`docid`),
  KEY `rc_sect_idx` (`secid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `richtings_coordinator`
--

INSERT INTO `richtings_coordinator` (`rcid`, `docid`, `secid`) VALUES
(1, 6, 4),
(2, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `schooljaar`
--

CREATE TABLE IF NOT EXISTS `schooljaar` (
  `sjid` int(11) NOT NULL AUTO_INCREMENT,
  `schooljaar` varchar(45) NOT NULL,
  PRIMARY KEY (`sjid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `schooljaar`
--

INSERT INTO `schooljaar` (`sjid`, `schooljaar`) VALUES
(1, '2010 - 2011'),
(2, '2011 - 2012'),
(3, '2012 - 2013'),
(4, '2013 - 2014'),
(5, '2014 - 2015');

-- --------------------------------------------------------

--
-- Table structure for table `sectoren`
--

CREATE TABLE IF NOT EXISTS `sectoren` (
  `secid` int(11) NOT NULL AUTO_INCREMENT,
  `sector` varchar(45) NOT NULL,
  PRIMARY KEY (`secid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `sectoren`
--

INSERT INTO `sectoren` (`secid`, `sector`) VALUES
(1, 'Techniek'),
(2, 'Natuur Techniek'),
(3, 'Analisten'),
(4, 'CGO');

-- --------------------------------------------------------

--
-- Table structure for table `studenten`
--

CREATE TABLE IF NOT EXISTS `studenten` (
  `studid` int(11) NOT NULL AUTO_INCREMENT,
  `sjid` int(11) NOT NULL,
  `naam` varchar(45) NOT NULL,
  `voornaam` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `geslachtid` int(11) NOT NULL,
  PRIMARY KEY (`studid`),
  KEY `student_inschrijvj_idx` (`sjid`),
  KEY `student_geslacht_idx` (`geslachtid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `studenten`
--

INSERT INTO `studenten` (`studid`, `sjid`, `naam`, `voornaam`, `email`, `geslachtid`) VALUES
(1, 2, 'Bhoendie', 'Akshay', 'akshaybhoendie@gmail.com', 1),
(2, 2, 'Dongo', 'Frank', 'frankdongo@hotmail.com', 1),
(3, 2, 'Menso', 'Dean', 'deanmenso@gmail.com', 1),
(4, 2, 'Muyden', 'Jorge', 'jorgemuyden@gmail.com', 1),
(5, 2, 'Johns', 'Gene', 'genejohns@gmail.com', 1),
(6, 2, 'Ramadien', 'Sunil', 'sunilramadien@gmail.com', 1),
(7, 2, 'Lo An Njoe', 'Loraine', 'loraineloannjo@gmail.com', 2),
(8, 2, 'Picado', 'Rochelle', 'rochellepicado@gmail.com', 2),
(9, 2, 'Wilson', 'Pamela', 'pamelawilson@gmail.com', 2),
(11, 2, 'Dikoen', 'Quincy', 'dikoenquincy@gmai.com', 1),
(12, 2, 'Djokarijo', 'Roche', 'rochedjokarijo@gmail.com', 1),
(13, 2, 'Joemanbaks', 'Saeed', 'saeedjoeman@gmailcom', 1);

-- --------------------------------------------------------

--
-- Table structure for table `studenten_details`
--

CREATE TABLE IF NOT EXISTS `studenten_details` (
  `studdetid` int(11) NOT NULL AUTO_INCREMENT,
  `studid` int(11) NOT NULL,
  `klasid` int(11) NOT NULL,
  PRIMARY KEY (`studdetid`),
  KEY `stud_studdet_idx` (`studid`),
  KEY `stud_klas_idx` (`klasid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `studenten_details`
--

INSERT INTO `studenten_details` (`studdetid`, `studid`, `klasid`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 2),
(6, 6, 2),
(7, 7, 2),
(8, 8, 3),
(9, 9, 3),
(10, 11, 3),
(11, 12, 4),
(12, 13, 4);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `afspraken`
--
ALTER TABLE `afspraken`
  ADD CONSTRAINT `afspr_pres` FOREIGN KEY (`presid`) REFERENCES `presentie` (`presid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `behandeld`
--
ALTER TABLE `behandeld`
  ADD CONSTRAINT `behand_pres` FOREIGN KEY (`presid`) REFERENCES `presentie` (`presid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `docenten`
--
ALTER TABLE `docenten`
  ADD CONSTRAINT `docent_geslacht` FOREIGN KEY (`geslid`) REFERENCES `geslacht` (`geslachtid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `klassen`
--
ALTER TABLE `klassen`
  ADD CONSTRAINT `klas_doc` FOREIGN KEY (`docid`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `klas_leerj` FOREIGN KEY (`ljid`) REFERENCES `leerjaren` (`ljid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `klas_schoolj` FOREIGN KEY (`sjid`) REFERENCES `schooljaar` (`sjid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `klas_sector` FOREIGN KEY (`secid`) REFERENCES `sectoren` (`secid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `logindata`
--
ALTER TABLE `logindata`
  ADD CONSTRAINT `login_bev` FOREIGN KEY (`bevid`) REFERENCES `bevoegdheden` (`bevid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `login_doc` FOREIGN KEY (`docid`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `memo_opmerkingen`
--
ALTER TABLE `memo_opmerkingen`
  ADD CONSTRAINT `docenten` FOREIGN KEY (`docentid`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `studenten` FOREIGN KEY (`studdetid`) REFERENCES `studenten_details` (`studdetid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `niet_verzorgd`
--
ALTER TABLE `niet_verzorgd`
  ADD CONSTRAINT `presid` FOREIGN KEY (`presid`) REFERENCES `presentie` (`presid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `opmerkingen`
--
ALTER TABLE `opmerkingen`
  ADD CONSTRAINT `opm_doc` FOREIGN KEY (`docid`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `opm_klas` FOREIGN KEY (`klasid`) REFERENCES `klassen` (`klasid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `opm_stud` FOREIGN KEY (`studid`) REFERENCES `studenten_details` (`studdetid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `presentie`
--
ALTER TABLE `presentie`
  ADD CONSTRAINT `les1_status` FOREIGN KEY (`les_1`) REFERENCES `les_status` (`lesstid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `les2_status` FOREIGN KEY (`les_2`) REFERENCES `les_status` (`lesstid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `les3_status` FOREIGN KEY (`les_3`) REFERENCES `les_status` (`lesstid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `les4_status` FOREIGN KEY (`les_4`) REFERENCES `les_status` (`lesstid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `les5_status` FOREIGN KEY (`les_5`) REFERENCES `les_status` (`lesstid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `les6_status` FOREIGN KEY (`les_6`) REFERENCES `les_status` (`lesstid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `les7_status` FOREIGN KEY (`les_7`) REFERENCES `les_status` (`lesstid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `les8_status` FOREIGN KEY (`les_8`) REFERENCES `les_status` (`lesstid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres_doc1` FOREIGN KEY (`docid_1`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres_doc2` FOREIGN KEY (`docid_2`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres_doc3` FOREIGN KEY (`docid_3`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres_doc4` FOREIGN KEY (`docid_4`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres_doc5` FOREIGN KEY (`docid_5`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres_doc6` FOREIGN KEY (`docid_6`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres_doc7` FOREIGN KEY (`docid_7`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres_doc8` FOREIGN KEY (`docid_8`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres_klas` FOREIGN KEY (`klasid`) REFERENCES `klassen` (`klasid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres_stud` FOREIGN KEY (`studid`) REFERENCES `studenten_details` (`studdetid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `presentie_details`
--
ALTER TABLE `presentie_details`
  ADD CONSTRAINT `pres1_status` FOREIGN KEY (`les_1`) REFERENCES `presentie_status` (`presstatid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres2_status` FOREIGN KEY (`les_2`) REFERENCES `presentie_status` (`presstatid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres3_status` FOREIGN KEY (`les_3`) REFERENCES `presentie_status` (`presstatid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres4_status` FOREIGN KEY (`les_4`) REFERENCES `presentie_status` (`presstatid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres5_status` FOREIGN KEY (`les_5`) REFERENCES `presentie_status` (`presstatid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres6_status` FOREIGN KEY (`les_6`) REFERENCES `presentie_status` (`presstatid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres7_status` FOREIGN KEY (`les_7`) REFERENCES `presentie_status` (`presstatid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pres8_status` FOREIGN KEY (`les_8`) REFERENCES `presentie_status` (`presstatid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `presdet_pres` FOREIGN KEY (`presid`) REFERENCES `presentie` (`presid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `presdet_stud` FOREIGN KEY (`studid`) REFERENCES `studenten_details` (`studdetid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `richtings_coordinator`
--
ALTER TABLE `richtings_coordinator`
  ADD CONSTRAINT `rc_doc` FOREIGN KEY (`docid`) REFERENCES `docenten` (`docid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `rc_sect` FOREIGN KEY (`secid`) REFERENCES `sectoren` (`secid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `studenten`
--
ALTER TABLE `studenten`
  ADD CONSTRAINT `student_geslacht` FOREIGN KEY (`geslachtid`) REFERENCES `geslacht` (`geslachtid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_inschrijvj` FOREIGN KEY (`sjid`) REFERENCES `schooljaar` (`sjid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `studenten_details`
--
ALTER TABLE `studenten_details`
  ADD CONSTRAINT `stud_klas` FOREIGN KEY (`klasid`) REFERENCES `klassen` (`klasid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `stud_studdet` FOREIGN KEY (`studid`) REFERENCES `studenten` (`studid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
