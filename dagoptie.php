<?php session_start();
	include ("inc/db.php");
	
	if(!isset($_SESSION['gebruikersnaam'])) {
		header('Location: index.php');
	}?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>LVS | Dagstaat Optie</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no">
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/stuck.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/ihover.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.1.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/tmStickUp.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script>
			$(document).ready(function(){
			 $().UItoTop({ easingType: 'easeOutQuart' });
			 $('#stuck_container').tmStickUp({});
			 });
		</script>
		<!--[if lt IE 9]>
		<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
			</a>
		</div>
		<script src="js/html5shiv.js"></script>
		<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
		<!--[if lt IE 10]>
		<link rel="stylesheet" media="screen" href="css/ie1.css">
		<![endif]-->
	</head>
	<body class="page1" id="top">
		<!--==============================
			header
			=================================-->
		<header>
			<!--==============================
				Stuck menu
				=================================-->
			<?php include ("inc/header.php"); ?>
		</header>
		<!--=====================
			Content
			======================-->
		<section class="content">
			<div class="ic">More Website Templates @ TemplateMonster.com - June 16, 2014!</div>
			<div class="container">
				<div class="row">
					<p>
						<center><h1 style="font-size: 30px;">DAGSTAAT OPTIES </h1></center>
					</p>
					<hr>
					<div class="grid_3" style="margin-left: 393px;">
						<div class="ih-item circle effect2 left_to_right">
							<a href="dagstaat.php">
								<div class="img"><img src="images/circ_img5.jpg" alt="img"></div>
								<div class="info">
									<h3>Invullen</h3>
									<p>dagstaat invullen</p>
								</div>
							</a>
						</div>
					</div>
					<div class="grid_3">
						<div class="ih-item circle effect2 left_to_right">
							<a href="dagbekijk.php">
								<div class="img"><img src="images/circ_img5.jpg" alt="img"></div>
								<div class="info">
									<h3>Bekijken</h3>
									<p>dagstaat bekijken</p>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--==============================
			footer
			=================================-->
		<?php include ("inc/footer.php"); ?>
	</body>
</html>