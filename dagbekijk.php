<?php session_start();
	include ("inc/db.php");
	
	if(!isset($_SESSION['gebruikersnaam'])) {
		header('Location: index.php');
	}
	$date = time();
	if(!isset($_GET['jaar'])) {
		$jaar = date('Y', $date);
	} else {
		$jaar = $_GET['jaar'];
	}
	
	if(!isset($_GET['maand'])) {
		$maand = date('m', $date);
	} else {
		$maand = $_GET['maand'];
		if($_GET['maand'] == 0) {
			$maand = 12;
			$jaar = $_GET['jaar'] - 1;
			$link = "dagbekijk.php?klas=".$_GET['klas']."&maand=".$maand."&jaar=".$jaar;			
			header('Location: '.$link);
		}
		
		if($_GET['maand'] == 13) {
			$maand = 1;
			$jaar = $_GET['jaar'] + 1;
			$link = "dagbekijk.php?klas=".$_GET['klas']."&maand=".$maand."&jaar=".$jaar;			
			header('Location: '.$link);
		}
	}
	?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>LVS | Dagstaat Bekijken</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no">
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/stuck.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/ihover.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.1.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/tmStickUp.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script type="text/javascript" src="js/css-pop.js"></script>
		<script>
			$(document).ready(function(){
			 $().UItoTop({ easingType: 'easeOutQuart' });
			 $('#stuck_container').tmStickUp({});
			 });
		</script>
		<!--[if lt IE 9]>
		<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
			</a>
		</div>
		<script src="js/html5shiv.js"></script>
		<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
		<!--[if lt IE 10]>
		<link rel="stylesheet" media="screen" href="css/ie1.css">
		<![endif]-->
	</head>
	<body class="page1" id="top" onload="popup('popUpDiv')">
		<header>
			<!--==============================
				Header
				=================================-->
			<?php include ("inc/header.php"); ?>
		</header>
		
		<?php
			//Docent ID en Sector ID opvragen
			$docentid = $_SESSION['gebruikersid'];
			if($_SESSION['bevid'] == 3) {
				$query_dagstaat = "SELECT * FROM klassen WHERE docid='$docentid'";
			} else if($_SESSION['bevid'] == 4) {
				$secid = $_SESSION['secid'];
				$query_dagstaat = "SELECT * FROM klassen WHERE secid='$secid'";
			}
			$result_dagstaat = mysqli_query($db, $query_dagstaat);
			$count_dagstaat = mysqli_num_rows($result_dagstaat);
			if(!isset($_GET['klas']) && $count_dagstaat > 1) {?>
				<div id="blanket" style="display:none;"></div>
				<div id="popUpDiv" style="display:none; top:250px;">			
				<form action="dagbekijk.php" method="GET">
					<div style="background:rgb(16, 70, 16); color: white;font-size: 20px;padding: 10px;font-weight: bold;"> <center>Kies een klas</center> </div>	
					<center>
						<div style="margin-top: 20px;">Dagstaat Invullen voor Klas: 
							<select name="klas">
								<?php
								while($data_dagstaat = mysqli_fetch_array($result_dagstaat)) {?>
									<option value="<?php echo $data_dagstaat['klasid']; ?>"><strong><?php echo $data_dagstaat['klas']; ?></strong></option>
								<?php
								}
								?>
							</select>
						</div>
					</center>
					<div style ="float: right; margin-right:50px; margin-top: 10px;"> 
						<input type="submit" value="Kiezen" onclick="popup('popUpDiv')" style="width: 70px; height: 35px; padding-left: 20px;padding-right: 20px;padding-top: 5px;padding-bottom: 5px;background: rgb(16, 70, 16); color: white;border: 0px;" >
					</div>			
				</form>
				</div>	
			<?php
			} else if(!isset($_GET['klas']) && $count_dagstaat == 1) {
				$data_dagstaat = mysqli_fetch_array($result_dagstaat);
				$klasid = $data_dagstaat['klasid'];
				header('Location: dagbekijk.php?klas='.$klasid);
			}?>
		<section class="content">
			<div class="ic">More Website Templates @ TemplateMonster.com - June 16, 2014!</div>
			<div class="container">
				<div class="row">
					<p>
						<center><h1 style="font-size: 30px;">DAGSTAAT BEKIJKEN </h1></center>
					</p>
					<div style="float:left;"><a href="dagbekijk.php?klas=<?php echo $_GET['klas'];?>&maand=<?php echo $maand-1;?>&jaar=<?php echo $jaar;?>"><<<<< Vorige Maand</a></div>
					<div style="float:right;"><a href="dagbekijk.php?klas=<?php echo $_GET['klas'];?>&maand=<?php echo $maand+1;?>&jaar=<?php echo $jaar;?>">Volgende Maand >>>>></a></div>
					<br />
					<hr>
					<?php
					if(!isset($_GET['datum'])) {
						if(isset($_GET['klas'])) {
							include ("inc/calendar.php");
						}
					} else {
						$datum = $_GET['datum'];
						if(isset($_GET['klas'])) {
							$klasid = $_GET['klas'];
						} else {
							$klasid = $_SESSION['klas'];
						}
						
						$query_dagstaat= "SELECT * FROM presentie INNER JOIN afspraken ON presentie.presid=afspraken.presid INNER JOIN behandeld ON presentie.presid=behandeld.presid WHERE presentie.datum='$datum' AND presentie.klasid='$klasid'";
						$result_dagstaat = mysqli_query($db, $query_dagstaat);
						$data_dagstaat = mysqli_fetch_array($result_dagstaat);
						if(mysqli_num_rows($result_dagstaat)==0) {
							header('location: dagbekijk.php');
						}
						$query_klas= "SELECT * FROM klassen WHERE klasid='$klasid'";
						$result_klas = mysqli_query($db, $query_klas);
						$data_klas = mysqli_fetch_array($result_klas);
						
						$koid = $data_dagstaat['studid'];
						$query_ko= "SELECT * FROM studenten_details INNER JOIN presentie ON studenten_details.studdetid=presentie.studid INNER JOIN studenten ON studenten_details.studid=studenten.studid WHERE studenten_details.studdetid='$koid'";
						$result_ko = mysqli_query($db, $query_ko);
						$data_ko = mysqli_fetch_array($result_ko);
						?>
						Klas: <strong><?php echo $data_klas['klas']; ?></strong> &nbsp;&nbsp;&nbsp; <strong>A.O</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Week: <strong><?php echo $data_dagstaat['week']; ?></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Dag: <strong><?php echo $data_dagstaat['dag']; ?></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Datum: <strong><?php echo $data_dagstaat['datum']; ?></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Naam KO: <strong><?php echo $data_ko['voornaam']." ".$data_ko['naam']; ?></strong> &nbsp;&nbsp;&nbsp;
						<?php
						$query_kd= "SELECT * FROM klassen INNER JOIN docenten ON klassen.docid=docenten.docid WHERE klassen.klasid='$klasid'";
						$result_kd = mysqli_query($db, $query_kd);
						$data_kd = mysqli_fetch_array($result_kd);
						?>
						Klassendocent: <strong><?php echo $data_kd['voornaam']." ".$data_kd['naam']; ?></strong>
						<br>
						<br>
						<div style="float:left;">
							<style type="text/css">
								.tg  {border-collapse:collapse;border-spacing:0;}
								.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg .tg-s6z2{text-align:center}
								.tg .tg-e3zv{font-weight:bold}
								.tg .tg-hgcj{font-weight:bold;text-align:center}
							</style>
							<table class="tg" style="undefined;table-layout: fixed; width: 638px">
								<colgroup>
									<col style="width: 52px">
									<col style="width: 97px">
									<col style="width: 102px">
									<col style="width: 51px">
									<col style="width: 55px">
									<col style="width: 55px">
									<col style="width: 55px">
									<col style="width: 55px">
									<col style="width: 55px">
									<col style="width: 55px">
									<col style="width: 55px">
									<col style="width: 55px">
								</colgroup>
								<tbody>
									<tr>
										<th class="tg-031e"></th>
										<th class="tg-e3zv">Naam</th>
										<th class="tg-e3zv">Voornaam</th>
										<th class="tg-e3zv">M/V</th>
										<?php
										for($i=1;$i<=8;$i++) {?>
											<th class="tg-hgcj"><?php echo $i;?></th>
										<?php
										}
										?>
									</tr>
									<?php
									$presid = $data_dagstaat['presid'];
									$query_presendet= "SELECT * FROM presentie_details WHERE presid='$presid'";
									$result_presendet = mysqli_query($db, $query_presendet);
									$i = 1;
									while($data_presendet = mysqli_fetch_array($result_presendet)) {
										$studid = $data_presendet['studid'];
										$query_studentgeg = "SELECT * FROM studenten_details INNER JOIN studenten ON studenten_details.studid=studenten.studid WHERE studdetid='$studid'";
										$result_studentgeg = mysqli_query($db, $query_studentgeg);
										$data_studentgeg = mysqli_fetch_array($result_studentgeg);?>
										<tr>
											<td class="tg-s6z2"><?php echo $i;?></td>
											<td class="tg-031e"><?php echo $data_studentgeg['naam'];?></td>
											<td class="tg-031e"><?php echo $data_studentgeg['voornaam'];?></td>
											<td class="tg-031e">M</td>
											<?php
											for($ii=1;$ii<=8;$ii++) {
												$les_status = $data_presendet['les_'.$ii];
												$query_lesgeg = "SELECT * FROM presentie_status WHERE presstatid='$les_status'";
												$result_lesgeg = mysqli_query($db, $query_lesgeg);
												$data_lesgeg = mysqli_fetch_array($result_lesgeg);?>
												<td class="tg-031e">
													<?php echo $data_lesgeg['status'];?></td>
											<?php
											}
											?>
										</tr>
									<?php
										$i++;
									}
									?>
								</tbody>
							</table>
						</div>
						<div style="float:right;">
							<style type="text/css">
								.tg  {border-collapse:collapse;border-spacing:0;}
								.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg .tg-s6z2{text-align:center}
								.tg .tg-hgcj{font-weight:bold;text-align:center}
							</style>
							<table class="tg" style="undefined;table-layout: fixed; width: 364px">
								<colgroup>
									<col style="width: 61px">
									<col style="width: 239px">
									<col style="width: 64px">
								</colgroup>
								<tbody>
									<tr>
										<th class="tg-hgcj">Lesuur</th>
										<th class="tg-hgcj">Behandelde Stof</th>
										<th class="tg-hgcj">Paraaf</th>
									</tr>
									<?php
									for($i=1;$i<=8;$i++) {?>
										<tr>
											<td class="tg-s6z2"><?php echo $i; ?></td>
											<td class="tg-031e">
												<?php if($data_dagstaat['behandeld_'.$i]=="") {
													 echo "--------------------------"; 
												} else {
													 echo $data_dagstaat['behandeld_'.$i]; 
												}?>
											</td>
											<?php
											$docent_bh = $data_dagstaat['docid_'.$i];
											$query_bh = "SELECT * FROM docenten WHERE docid='$docent_bh'";
											$result_bh = mysqli_query($db, $query_bh);
											$data_bh = mysqli_fetch_array($result_bh);
											?>
											<td><?php echo substr($data_bh['voornaam'], 0, 1).", ".substr($data_bh['naam'], 0, 4);?></td>
											
										</tr>
									<?php
									}?>
								</tbody>
							</table>
						</div>
						<br><br><br>
						<div style="float:right;margin-top: 410px;margin-right: -365px;">
							<style type="text/css">
								.tg  {border-collapse:collapse;border-spacing:0;}
								.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
								.tg .tg-s6z2{text-align:center}
								.tg .tg-hgcj{font-weight:bold;text-align:center}
							</style>
							<table class="tg" style="undefined;table-layout: fixed; width: 314px">
								<colgroup>
									<col style="width: 61px">
									<col style="width: 303px">
								</colgroup>
								<tbody>
									<tr>
										<th class="tg-hgcj">Lesuur</th>
										<th class="tg-hgcj">Afspraken</th>
									</tr>
									<?php
									for($i=1;$i<=8;$i++) {?>
										<tr>
											<td class="tg-s6z2"><?php echo $i; ?></td>
											<td class="tg-031e">
												<?php if($data_dagstaat['afspraak_'.$i]=="") {
													 echo "--------------------------"; 
												} else {
													 echo $data_dagstaat['afspraak_'.$i]; 
												}?>
											</td>
										</tr>
									<?php
									}?>
								</tbody>
							</table>
						</div>
						<br>
						<div style="float:left; margin-top:30px;">
							<style type="text/css">
								.tg {
									border-collapse: collapse;
									border-spacing: 0;
								}
								.tg td {
									font-family: Arial, sans-serif;
									font-size: 14px;
									padding: 10px 5px;
									border-style: solid;
									border-width: 1px;
									overflow: hidden;
									word-break: normal;
								}
								.tg th {
									font-family: Arial, sans-serif;
									font-size: 14px;
									font-weight: normal;
									padding: 10px 5px;
									border-style: solid;
									border-width: 1px;
									overflow: hidden;
									word-break: normal;
								}
								.tg .tg-hgcj {
									font-weight: bold;
									text-align: center
								}
							</style>
							<table class="tg" style="undefined;table-layout: fixed; width: 650px">
								<colgroup>
									<col style="width: 131px">
									<col style="width: 107px">
									<col style="width: 63px">
									<col style="width: 63px">
									<col style="width: 63px">
									<col style="width: 63px">
									<col style="width: 63px">
									<col style="width: 63px">
									<col style="width: 63px">
									<col style="width: 63px">
								</colgroup>
								<tbody>
									<tr>
										<th class="tg-031e" rowspan="2">Gevolgt = G
										<br>
										Afwezig = A
										<br>
										Ziek &nbsp; &nbsp; &nbsp;= Z
										<br>
										Laat &nbsp; &nbsp; &nbsp;= L</th>
										<th class="tg-hgcj">Vak</th>
										<?php
										for($i=1;$i<=8;$i++) {?>
											<th class="tg-031e"><?php echo $data_dagstaat['vak_'.$i];?></th>
										<?php
										}?>
									</tr>
									<tr>
										<td class="tg-hgcj">Paraaf</td>
										<?php
										for($i=1;$i<=8;$i++) {
											$docent_gv = $data_dagstaat['docid_'.$i];
											$query_gv = "SELECT * FROM docenten WHERE docid='$docent_gv'";
											$result_gv = mysqli_query($db, $query_gv);
											$data_gv = mysqli_fetch_array($result_gv);
											?>
											<td class="tg-031e"><?php echo substr($data_bh['voornaam'], 0, 1).", ".substr($data_bh['naam'], 0, 4);?></td>
										<?php
										}?>
									</tr>
								</tbody>
							</table>
						</div>
					<?php
					}?>
				</div>
			</div>
		</section>
		<!--==============================
			footer
			=================================-->
		<?php include ("inc/footer.php"); ?>
	</body>
</html>