<?php session_start();
	include ("inc/db.php");
	
	if(!isset($_SESSION['gebruikersnaam'])) {
		header('Location: index.php');
	}?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>LVS | Vertrekbrief</title>
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no">
<link rel="icon" href="images/favicon.ico">
<link rel="shortcut icon" href="images/favicon.ico">
<link rel="stylesheet" href="css/stuck.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/ihover.css">
<script src="js/jquery.js"></script>
<script src="js/jquery-migrate-1.1.1.js"></script>
<script src="js/script.js"></script>
<script src="js/superfish.js"></script>
<script src="js/jquery.equalheights.js"></script>
<script src="js/jquery.mobilemenu.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/tmStickUp.js"></script>
<script src="js/jquery.ui.totop.js"></script>
<script>
 $(document).ready(function(){
  $().UItoTop({ easingType: 'easeOutQuart' });
  $('#stuck_container').tmStickUp({});
  });
</script>
<!--[if lt IE 9]>
 <div style=' clear: both; text-align:center; position: relative;'>
   <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
     <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
   </a>
</div>
<script src="js/html5shiv.js"></script>
<link rel="stylesheet" media="screen" href="css/ie.css">
<![endif]-->
<!--[if lt IE 10]>
<link rel="stylesheet" media="screen" href="css/ie1.css">
<![endif]-->
</head>
<body class="page1" id="top">
<!--==============================
              header
=================================-->
<header>
<!--==============================
            Stuck menu
=================================-->
<?php include ("inc/header.php"); ?>
</header>
<!--=====================
          Content
======================-->
<section class="content"><div class="ic">More Website Templates @ TemplateMonster.com - June 16, 2014!</div>

  <div class="container">
  <div >
  <p><h1 style="font-size: 30px;">Briefje voor vroegtijdig vertrek van een student</h1></p>

<table border="1">
	<BR>
			<table border="1">	
						<p><h1 style="font-size: 20px;">A. 	IN TE VULLEN DOOR STUDENT</h1></p>					
						<tr>
							
							<th>Naam en voor letters</th>
							<th>Klas</th>
							<th>Reden</th>
							<th>Datum:</th>
							<th>Uur van vertrek</th>
						</tr>

									<tr>
																	
										<td><input type="text" placeholder="Naam en voor letters"></td>
										
										<td><input type="text" placeholder="Klas"></td>
										
										<td><input type="text" placeholder="Reden"></td>
										
										<td><input type="date" name="datum" required=""></td>
										<td><input name="Uur van vertrek" required=""></td>
									</tr>
									
																
			</table>
			
			<table border="1">	
						<p><h1 style="font-size: 20px;">B. 	IN TE VULLEN DOOR KLASSENDOCENT/DIRECTIELID</h1></p>
						<p><h1 style="font-size: 18px;">Gezien en akkoord klassendocent/directielid:</h1></p>
						<tr>
							
							<th>Naam</th>
							<th>Datum:</th>
							
						</tr>

									<tr>
																	
										<td><input type="text" placeholder="Naam"></td>
										
										<td><input type="date" name="datum" required=""></td>
										
									</tr>
									
																
			</table>
			
						<table border="1">	
						<p><h1 style="font-size: 20px;">C. 	IN TE VULLEN DOOR OUDERS/VERZORGERS</h1></p>
						<p><h1 style="font-size: 18px;">Gezien en akkoord ouder/verzorger:</h1></p>
						<tr>
							
							<th>Naam en voor letters</th>
							<th>Datum:</th>
							
						</tr>

									<tr>
																	
										<td><input type="text" placeholder="Naam en voor letters"></td>
										
										<td><input type="date" name="datum" required=""></td>
										
									</tr>
									
																
			</table>
			
			
			
					
</table>
					
					
				<br>
							<input type="submit" value="Opslaan" class="button" name="submit">
	
	<br>
	
	
	
</div>	
	
  </div>
</section>
<!--==============================
              footer
=================================-->
<?php include ("inc/footer.php"); ?>
</body>
</html>

