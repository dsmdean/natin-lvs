<?php session_start();
	include ("inc/db.php");
	
	if(!isset($_SESSION['gebruikersnaam'])) {
		header('Location: index.php');
	}?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>LVS | Index</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no">
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/stuck.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/ihover.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.1.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/tmStickUp.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script>
			$(document).ready(function(){
			 $().UItoTop({ easingType: 'easeOutQuart' });
			 $('#stuck_container').tmStickUp({});
			 });
		</script>
		<!--[if lt IE 9]>
		<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
			</a>
		</div>
		<script src="js/html5shiv.js"></script>
		<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
		<!--[if lt IE 10]>
		<link rel="stylesheet" media="screen" href="css/ie1.css">
		<![endif]-->
	</head>
	<body class="page1" id="top">
		<!--==============================
			header
			=================================-->
		<header>
			<!--==============================
				Stuck menu
				=================================-->
			<?php include ("inc/header.php"); ?>
		</header>
		<!--=====================
			Content
			======================-->
		<section class="content">
			<div class="ic">More Website Templates @ TemplateMonster.com - June 16, 2014!</div>
			<center>
				<div class="container">
					<p>
					<h1 style="font-size: 30px;">DAGSTAAT VERZORGDE LESSEN SCHOOLJAAR 2013/2014 </h1>
					</p>
					<hr>
					Klas: <b>3.10.2</b> &nbsp;&nbsp;&nbsp; <b>A.O</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Week: <b> 1 </b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dag: <b>Zaterdag</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Datum: <b><?php echo date("d/m/Y");?></b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Naam KO: <b> Muyden </b> &nbsp;&nbsp;&nbsp; Klassendocent: <b>Vliese</b>
					<br><br>
					<style type="text/css">
						.tg  {border-collapse:collapse;border-spacing:0;}
						.tg td{font-family:Arial, sans-serif;font-size:11px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
						.tg th{font-family:Arial, sans-serif;font-size:12px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
						.tg .tg-s6z2{text-align:center}
						.tg .tg-e3zv{font-weight:bold}
						.tg .tg-hgcj{font-weight:bold;text-align:center;font-family:Arial, sans-serif;font-size:12px;}
					</style>
					<table class="tg" style="undefined;table-layout: fixed; width: 638px">
						<colgroup>
							<col style="width: 17px">
							<col style="width: 97px">
							<col style="width: 102px">
							<col style="width: 40px">
							<col style="width: 57px">
							<col style="width: 25px">
							<col style="width: 25px">
							<col style="width: 8px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 8px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 8px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 8px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 8px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 28px">
							<col style="width: 8px">
							<col style="width: 55px">
						</colgroup>
						<tr>
							<th class="tg-031e"></th>
							<th class="tg-e3zv">Naam</th>
							<th class="tg-e3zv">Voornaam</th>
							<th class="tg-e3zv">M/V</th>
							<th class="tg-hgcj">Diploma</th>
							<th class="tg-hgcj">z</th>
							<th class="tg-hgcj">d</th>
							<th class="tg-hgcj" rowspan="3" style="background: white;"></th>
							<th class="tg-hgcj">M</th>
							<th class="tg-hgcj">D</th>
							<th class="tg-hgcj">W</th>
							<th class="tg-hgcj">D</th>
							<th class="tg-hgcj">V</th>
							<th class="tg-hgcj">Z</th>
							<th class="tg-hgcj" rowspan="3" style="background: white;"></th>
							<th class="tg-hgcj">M</th>
							<th class="tg-hgcj">D</th>
							<th class="tg-hgcj">W</th>
							<th class="tg-hgcj">D</th>
							<th class="tg-hgcj">V</th>
							<th class="tg-hgcj">Z</th>
							<th class="tg-hgcj" rowspan="3" style="background: white;"></th>
							<th class="tg-hgcj">M</th>
							<th class="tg-hgcj">D</th>
							<th class="tg-hgcj">W</th>
							<th class="tg-hgcj">D</th>
							<th class="tg-hgcj">V</th>
							<th class="tg-hgcj">Z</th>
							<th class="tg-hgcj" rowspan="3" style="background: white;"></th>
							<th class="tg-hgcj">M</th>
							<th class="tg-hgcj">D</th>
							<th class="tg-hgcj">W</th>
							<th class="tg-hgcj">D</th>
							<th class="tg-hgcj">V</th>
							<th class="tg-hgcj">Z</th>
							<th class="tg-hgcj" rowspan="3" style="background: white;"></th>
							<th class="tg-hgcj">M</th>
							<th class="tg-hgcj">D</th>
							<th class="tg-hgcj">W</th>
							<th class="tg-hgcj">D</th>
							<th class="tg-hgcj">V</th>
							<th class="tg-hgcj">Z</th>
							<th class="tg-hgcj" rowspan="3" style="background: white;"></th>
							<th class="tg-hgcj">Totaal</th>
						</tr>
						<tr>
							<td class="tg-s6z2">1</td>
							<td class="tg-031e">Bhoendie</td>
							<td class="tg-031e">Akshay</td>
							<td class="tg-031e">M</td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
							<td class="tg-031e"></td>
						</tr>
					</table>
				</div>
				</div>
			</center>
		</section>
		<!--==============================
			footer
			=================================-->
		<?php include ("inc/footer.php"); ?>
	</body>
</html>