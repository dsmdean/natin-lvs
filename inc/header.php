<section id="stuck_container">
    <div class="container">
		<div class="row">
			<div class="grid_12">
				<h1>
				<a href="rc.php">
					<img src="images/logo.png" alt="Logo alt">
				</a>
				</h1>
				<div class="navigation ">
					<div style="float:right;margin-top: -61px;background: green;color: white;">
						<a href ="logout.php">Logout</a>
					</div>
					<?php
					if($_SESSION['bevid'] == 4) {?>
						<nav>
							<ul class="sf-menu">
								<li><a href="rc.php">Home</a></li>
								<li><a href="studenten.php">Studenten</a></li>
								<li><a href="klassen.php">Klassen</a></li>
								<li><a href="presentie.php">Presentie</a></li>
							</ul>
						</nav>
					<?php
					} else if($_SESSION['bevid'] == 3) {?>
						<nav>
							<ul class="sf-menu">
								<li><a href="rc.php">Home</a></li>
								<li><a href="studenten.php">Studenten</a></li>
								<li><a href="klassen.php">Klassen</a></li>
								<li><a href="presentie.php">Presentie</a></li>
							</ul>
						</nav>
					<?php
					} else if($_SESSION['bevid'] == 5 || $_SESSION['bevid'] == 6) {?>
						<nav>
							<ul class="sf-menu">
								<li><a href="rc.php">Home</a></li>
								<li><a href="studenten.php">Studenten</a></li>
								<li><a href="klassen.php">Klassen</a></li>
							</ul>
						</nav>
					<?php
					}
					?>
					<div class="clear"></div>
				</div>
			</div>
		</div>
    </div>
</section>