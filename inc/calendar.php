<?php
$date = time();

$day = date('d', $date);
$m = date('m', $date);
if(isset($_GET['maand'])) {
	$month = $_GET['maand'];
} else {
	$month = $m;
}
if(isset($_GET['jaar'])) {
	$year = $_GET['jaar'];
} else {
	$year = date('Y', $date);
}

$first_day = mktime(0, 0, 0, $month, 1, $year);

$title = date('F', $first_day);
$day_of_week = date('D', $first_day);

switch($day_of_week) {
	case "Sun": $blank = 0; break;
	case "Mon": $blank = 1; break;
	case "Tue": $blank = 2; break;
	case "Wed": $blank = 3; break;
	case "Thu": $blank = 4; break;
	case "Fri": $blank = 5; break;
	case "Sat": $blank = 6; break;
}

$days_in_month = cal_days_in_month(0, $month, $year);

echo "<table>";
echo "<tr><th colspan=60> $title $year </th></tr>";
echo "<tr><td width=62><strong>Zondag</strong></td><td width=62><strong>Maandag</strong></td><td width=62><strong>Dinsdag</strong></td><td width=62><strong>Woensdag</strong></td><td width=62><strong>Donderdag</strong></td><td width=62><strong>Vrijdag</strong></td><td width=62><strong>Zaterdag</strong></td></tr>";

$day_count = 1;

echo "<tr>";

while ($blank > 0) {
	echo "<td></td>";
	$blank = $blank - 1;
	$day_count++;
}

$day_num = 1;

while($day_num <= $days_in_month) {
	$datum = $year ."-".$month."-".$day_num;
	$klasid = $_GET['klas'];
	$query_datum = "SELECT * FROM presentie WHERE datum = '$datum' AND klasid = '$klasid'";
	$result_datum = mysqli_query($db, $query_datum);
	$data_datum = mysqli_fetch_array($result_datum);
	$datum_rows = mysqli_num_rows($result_datum);
	
	if($datum_rows > 0) {
		$page = curPageURL()."&datum=".$datum;
		echo "<td><a href='$page' style='color:  blue;'>$day_num</a></td>";
	} else {
		$page = "dagstaat.php?klas=".$klasid."&datum=".$datum."&dag=".$day_count;
		if($_SESSION['bevid'] == 3) {
			if($day_count != 1) {
				echo "<td><a href='$page' style='color:  red;'>$day_num</a></td>";
			} else {
				echo "<td style='color:  grey;'>$day_num</td>";
			}
		} else if($_SESSION['bevid'] == 4) {
			if($day_count != 1) {
				echo "<td style='color:  red;'>$day_num</td>";
			} else {
				echo "<td style='color:  grey;'>$day_num</td>";
			}
		}	
	}
	
	$day_num++;
	$day_count++;
	
	if($day_count > 7) {
		echo "</tr><tr>";
		$day_count = 1;
	}
}
	
	while($day_count > 1 && $day_count <= 7) {
		echo "<td></td>";
		$day_count++;
	}

echo "</tr></table>";


?>