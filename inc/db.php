<?php

define('DB_HOST', 'localhost');
define('DB_USER', 'admin');
define('DB_PASS', 'root');
define('DB_NAME', 'natinlvs');
define('WEBSITE_URL', 'localhost/natin-lvs');

$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

if(!$db) {
  echo "<p style='color:red;'>Kan niet verbinden met de database</p>";
}

function curPageURL() {
 	$pageURL = 'http';
 	$pageURL .= "://";
 	if ($_SERVER["SERVER_PORT"] != "80") {
  		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 	} else {
  		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 	}
 	return $pageURL;
}
?>