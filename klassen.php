<?php session_start();
	include ("inc/db.php");
	
	if(!isset($_SESSION['gebruikersnaam'])) {
		header('Location: index.php');
	}?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>LVS | Klassen</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no">
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/stuck.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/ihover.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.1.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/tmStickUp.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script>
			$(document).ready(function(){
			 $().UItoTop({ easingType: 'easeOutQuart' });
			 $('#stuck_container').tmStickUp({});
			 });
		</script>
		<!--[if lt IE 9]>
		<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
			</a>
		</div>
		<script src="js/html5shiv.js"></script>
		<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
		<!--[if lt IE 10]>
		<link rel="stylesheet" media="screen" href="css/ie1.css">
		<![endif]-->
	</head>
	<body class="page1" id="top">
		<!--==============================
			header
			=================================-->
		<header>
			<!--==============================
				Stuck menu
				=================================-->
			<?php include ("inc/header.php"); ?>
		</header>
		<!--=====================
			Content
			======================-->
		<section class="content">
			<div class="ic">More Website Templates @ TemplateMonster.com - June 16, 2014!</div>
			<center>
				<div class="container">
					<?php 
					if($_SESSION['bevid'] == 3) {
					?>
						<p>
						<h1 style="font-size: 30px;">Klassen van <?php echo $_SESSION['volNaam'];?></h1>
						</p>
						<?php 
							$docid = $_SESSION['gebruikersid'];
							$query_klassen = "SELECT * FROM klassen INNER JOIN schooljaar ON klassen.sjid = schooljaar.sjid INNER JOIN sectoren ON klassen.secid = sectoren.secid WHERE docid='$docid'";
							$result_klassen = mysqli_query($db, $query_klassen);?>
						<table border="1">
							<tr>
								<th>Klasid</th>
								<th>Klassen</th>
								<th>Schooljaar</th>
								<th>Sector</th>
							</tr>
							<?php while($data_klassen = mysqli_fetch_array($result_klassen)) {
								$klasid = $data_klassen['klasid'];?>
								<tr>
									<td><a href="studenten.php?klasid=<?php echo $klasid;?>"><?php echo $klasid;?></a></td>
									<td><a href="studenten.php?klasid=<?php echo $klasid;?>"><?php echo $data_klassen['klas'];?></a></td>
									<td><a href="studenten.php?klasid=<?php echo $klasid;?>"><?php echo $data_klassen['schooljaar'];?></a></td>
									<td><a href="studenten.php?klasid=<?php echo $klasid;?>"><?php echo $data_klassen['sector'];?></a></td>
								</tr>
							<?php }?>
						</table>
						<p>&nbsp;</p>
					<?php
					} else if($_SESSION['bevid'] == 4) {?>
						<?php 
							$secid = $_SESSION['secid'];
							$docid = $_SESSION['gebruikersid'];
							$query_klassen = "SELECT * FROM klassen INNER JOIN schooljaar ON klassen.sjid = schooljaar.sjid INNER JOIN docenten ON klassen.docid = docenten.docid WHERE klassen.secid='$secid'";
							$result_klassen = mysqli_query($db, $query_klassen);?>
						<p>
						<h1 style="font-size: 30px;">Klassen van <?php echo $_SESSION['sector'];?></h1>
						</p>
						<table border="1">
							<tr>
								<th>Klasid</th>
								<th>Klas</th>
								<th>Schooljaar</th>
								<th>Klasse Docent</th>
							</tr>
							<?php while($data_klassen = mysqli_fetch_array($result_klassen)) {
								$klasid = $data_klassen['klasid'];?>
								<tr>
									<td><a href="studenten.php?klasid=<?php echo $klasid;?>"><?php echo $klasid;?></a></td>
									<td><a href="studenten.php?klasid=<?php echo $klasid;?>"><?php echo $data_klassen['klas'];?></a></td>
									<td><a href="studenten.php?klasid=<?php echo $klasid;?>"><?php echo $data_klassen['schooljaar'];?></a></td>
									<td><a href="studenten.php?klasid=<?php echo $klasid;?>"><?php echo $data_klassen['voornaam']." ".$data_klassen['naam'];?></a></td>
								</tr>
							<?php }?>
						</table>
						<p>&nbsp;</p>
					<?php
					} else if($_SESSION['bevid'] == 5 || $_SESSION['bevid'] == 6) {?>
						<?php 
							$docid = $_SESSION['gebruikersid'];
							$query_klassen = "SELECT * FROM klassen INNER JOIN schooljaar ON klassen.sjid = schooljaar.sjid INNER JOIN docenten ON klassen.docid = docenten.docid";
							$result_klassen = mysqli_query($db, $query_klassen);?>
						<p>
						<h1 style="font-size: 30px;">Klassen van <?php echo $_SESSION['sector'];?></h1>
						</p>
						<table border="1">
							<tr>
								<th>Klasid</th>
								<th>Klas</th>
								<th>Schooljaar</th>
								<th>Klasse Docent</th>
							</tr>
							<?php while($data_klassen = mysqli_fetch_array($result_klassen)) {
								$klasid = $data_klassen['klasid'];?>
								<tr>
									<td><a href="studenten.php?klasid=<?php echo $klasid;?>"><?php echo $klasid;?></a></td>
									<td><a href="studenten.php?klasid=<?php echo $klasid;?>"><?php echo $data_klassen['klas'];?></a></td>
									<td><a href="studenten.php?klasid=<?php echo $klasid;?>"><?php echo $data_klassen['schooljaar'];?></a></td>
									<td><a href="studenten.php?klasid=<?php echo $klasid;?>"><?php echo $data_klassen['voornaam']." ".$data_klassen['naam'];?></a></td>
								</tr>
							<?php }?>
						</table>
						<p>&nbsp;</p>
					<?php
					}
					?>
				</div>
			</center>
		</section>
		<!--==============================
			footer
			=================================-->
		<?php include ("inc/footer.php"); ?>
	</body>
</html>