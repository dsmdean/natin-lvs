<?php session_start();
	include ("inc/db.php");
	
	if(!isset($_SESSION['gebruikersnaam'])) {
		header('Location: index.php');
	}
	if(!isset($_GET['studdetid'])) {
		header('Location: studenten.php');
	}?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>LVS | Studentenkaart</title>
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no">
<link rel="icon" href="images/favicon.ico">
<link rel="shortcut icon" href="images/favicon.ico">
<link rel="stylesheet" href="css/stuck.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/ihover.css">
<script src="js/jquery.js"></script>
<script src="js/jquery-migrate-1.1.1.js"></script>
<script src="js/script.js"></script>
<script src="js/superfish.js"></script>
<script src="js/jquery.equalheights.js"></script>
<script src="js/jquery.mobilemenu.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/tmStickUp.js"></script>
<script src="js/jquery.ui.totop.js"></script>
<script>
 $(document).ready(function(){
  $().UItoTop({ easingType: 'easeOutQuart' });
  $('#stuck_container').tmStickUp({});
  });
</script>
<!--[if lt IE 9]>
 <div style=' clear: both; text-align:center; position: relative;'>
   <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
     <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
   </a>
</div>
<script src="js/html5shiv.js"></script>
<link rel="stylesheet" media="screen" href="css/ie.css">
<![endif]-->
<!--[if lt IE 10]>
<link rel="stylesheet" media="screen" href="css/ie1.css">
<![endif]-->
<!-- Print Option -->
<script>
	function printContent(printPage){
		var restorepage = document.body.innerHTML;
		var print = document.getElementById(printPage).innerHTML;
		document.body.innerHTML = print;
		window.print();
		document.body.innerHTML = restorepage;
	}
</script>

</head>
<body class="page1" id="top">
<!--==============================
              header
=================================-->
<header>
<!--==============================
            Stuck menu
=================================-->
<?php include ("inc/header.php"); ?>
</header>
<!--=====================
          Content
======================-->
<section class="content"><div class="ic">More Website Templates @ TemplateMonster.com - June 16, 2014!</div>
			<button onclick="printContent('printPage')" style="width: 57px; height: 57px; margin-top: 59px;/* padding-left: 20px; *//* padding-right: 20px; */padding-top: 5px;padding-bottom: 5px;background: rgb(16, 70, 16); color: white;border: 0px;position:fixed;text-align: center;"><div style="margin-top:2px;">Print </div><img src="images/print.png"></img></button>
			<div id="printPage" class="container">
  <div style= "width: 850px; margin-left:auto; margin-right:auto;">
  <p><h1 style="font-size: 30px; text-align:center;">Studentenkaart</h1></p>

	<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-uyc4{font-weight:bold;background-color:#104610;color:#ffffff}
.tg .tg-cxkv{background-color:#ffffff}
</style>
<center>
<?php
$studdetid = $_GET['studdetid'];
$query_student = "SELECT * FROM studenten INNER JOIN studenten_details ON studenten.studid = studenten_details.studid INNER JOIN klassen ON studenten_details.klasid = klassen.klasid INNER JOIN schooljaar ON klassen.sjid = schooljaar.sjid INNER JOIN geslacht ON studenten.geslachtid = geslacht.geslachtid WHERE studdetid='$studdetid'";
$result_student = mysqli_query($db, $query_student);
$data_student = mysqli_fetch_array($result_student);
?>
<table class="tg" style="undefined;table-layout: fixed; width: 500px">
<colgroup>
<col style="width: 130px">
<col style="width: 21px">
<col style="width: 49px">
<col style="width: 49px">
<col style="width: 112px">
</colgroup>
  <tr>
    <th class="tg-uyc4" colspan="2">Naam</th>
    <th class="tg-cxkv" colspan="3"><input type="text" value="<?php echo $data_student['voornaam']." ".$data_student['naam']; ?>" style="width:100%; margin-left:-2px;" disabled=""/></th>
  </tr>
  <tr>
    <th class="tg-uyc4" colspan="2">Geslacht</th>
    <th class="tg-cxkv" colspan="3"><input type="text" value="<?php echo $data_student['geslacht']; ?>" style="width:100%; margin-left:-2px;" disabled=""/></th>
  </tr>
  <tr>
    <td class="tg-uyc4" colspan="2">Schooljaar</td>
    <td class="tg-cxkv" colspan="3"><input type="text" value="<?php echo $data_student['schooljaar']; ?>" style="width:100%; margin-left:-2px;" disabled=""/></td>
  </tr>
  <tr>
    <td class="tg-uyc4" colspan="2">Sector</td>
    <td class="tg-cxkv" colspan="3"><input type="text" value="<?php 
    	if(isset($_SESSION['sector'])) {
    	 	echo $_SESSION['sector']; 
		} else {
    		$klasid = $data_student['klasid'];
			$query_sector = "SELECT * FROM klassen INNER JOIN sectoren ON klassen.secid = sectoren.secid WHERE klasid='$klasid'";
			$result_sector = mysqli_query($db, $query_sector);
			$data_sector = mysqli_fetch_array($result_sector);
			echo $data_sector['sector'];
		}?>" style="width:100%; margin-left:-2px;" disabled=""/></td>
  </tr>
  <tr>
    <td class="tg-uyc4" colspan="2">Klas</td>
    <td class="tg-cxkv" colspan="3"><input type="text" value="<?php echo $data_student['klas']; ?>" style="width:100%; margin-left:-2px;" disabled=""/></td>
  </tr>
  <tr>
  	<td colspan="5"><a href="memo_opmerking.php?studdetid=<?php echo $studdetid;?>&type=0"><button>Opmerkingen bekijken</button></a>&nbsp;<a href="memo_opmerking.php?studdetid=<?php echo $studdetid;?>&type=1"><button>Opmerking Plaatsen</button></a></td>
  </tr>
</table>

<br><br>
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-cxkv{background-color:#ffffff}
.tg .tg-2ro4{font-weight:bold;background-color:#ffffff;color:#000000}
.tg .tg-ju3u{font-weight:bold;background-color:#104610;color:#ffffff;text-align:center}
</style>
<table class="tg" style="undefined;table-layout: fixed; width: 813px">
    <th class="tg-ju3u">Verzuimde lesuren</th>
    <th class="tg-ju3u">Datum</th>
    <th class="tg-ju3u">Week</th>
    <th class="tg-ju3u">Dag</th>
    <th class="tg-ju3u">Lesuur</th>
  </tr>
  <?php
  $query_verzuimd = "SELECT * FROM presentie INNER JOIN presentie_details ON presentie.presid = presentie_details.presid WHERE presentie_details.studid='$studdetid' AND (presentie_details.les_1 = 2 OR presentie_details.les_2 = 2 OR presentie_details.les_3 = 2 OR presentie_details.les_4 = 2 OR presentie_details.les_5 = 2 OR presentie_details.les_6 = 2 OR presentie_details.les_7 = 2 OR presentie_details.les_8 = 2)";
  $result_verzuimd = mysqli_query($db, $query_verzuimd);
  $i = 1;
								
  while($data_verzuimd = mysqli_fetch_array($result_verzuimd)) {
  	$lesurenAfwezig = "";
  	for($jj=1;$jj<=8;$jj++) {
	  if($data_verzuimd['les_'.$jj] == 2) {
	  	if($lesurenAfwezig == "") {
	  		$lesurenAfwezig = $jj;
	  	} else {
	  		$lesurenAfwezig = $lesurenAfwezig.", ".$jj;
	  	}
	  }
	}?>
  	<tr>
      <td class="tg-031e"><?php echo $i;?></td>
      <td class="tg-031e"><?php echo $data_verzuimd['datum'];?></td>
      <td class="tg-031e"><?php echo $data_verzuimd['week'];?></td>
      <td class="tg-031e"><?php echo $data_verzuimd['dag'];?></td>
      <td class="tg-031e"><?php echo $lesurenAfwezig;?></td>
    </tr>
  <?php
  	$i++;
  }
  ?>
</table>
<br/><br/>
<table class="tg" style="undefined;table-layout: fixed; width: 813px">
    <th class="tg-ju3u">Laat</th>
    <th class="tg-ju3u">Datum</th>
    <th class="tg-ju3u">Week</th>
    <th class="tg-ju3u">Dag</th>
    <th class="tg-ju3u">Lesuur</th>
  </tr>
  <?php
  $query_laat = "SELECT * FROM presentie INNER JOIN presentie_details ON presentie.presid = presentie_details.presid WHERE presentie_details.studid='$studdetid' AND (presentie_details.les_1 = 4 OR presentie_details.les_2 = 4 OR presentie_details.les_3 = 4 OR presentie_details.les_4 = 4 OR presentie_details.les_5 = 4 OR presentie_details.les_6 = 4 OR presentie_details.les_7 = 4 OR presentie_details.les_8 = 4)";
  $result_laat = mysqli_query($db, $query_laat);
  $i = 1;
  
  while($data_laat = mysqli_fetch_array($result_laat)) {
  	$lesurenLaat = "";
  	for($j=1;$j<=8;$j++) {
	  if($data_laat['les_'.$j] == 4) {
	  	if($lesurenLaat == "") {
	  		$lesurenLaat = $j;
	  	} else {
	  		$lesurenLaat = $lesurenLaat.", ".$j;
	  	}
	  }
	}?>
  	<tr>
      <td class="tg-031e"><?php echo $i;?></td>
      <td class="tg-031e"><?php echo $data_laat['datum'];?></td>
      <td class="tg-031e"><?php echo $data_laat['week'];?></td>
      <td class="tg-031e"><?php echo $data_laat['dag'];?></td>
      <td class="tg-031e"><?php echo $lesurenLaat;?></td>
    </tr>
  <?php
  	$i++;
  }
  ?>
</table>

</center>	
	
</div>	
	
  </div>
</section>
<!--==============================
              footer
=================================-->
<?php include ("inc/footer.php"); ?>
</body>
</html>

