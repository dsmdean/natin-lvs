<?php session_start();
include ("inc/db.php");

if (isset($_SESSION['gebruikersnaam'])) {
	header('Location: rc.php');
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>LVS|Login</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no">
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/stuck.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/ihover.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.1.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/tmStickUp.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script>
			$(document).ready(function() {
				$().UItoTop({
					easingType : 'easeOutQuart'
				});
				$('#stuck_container').tmStickUp({});
			});
		</script>
		<!--[if lt IE 9]>
		<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
		<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
		</a>
		</div>
		<script src="js/html5shiv.js"></script>
		<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
		<!--[if lt IE 10]>
		<link rel="stylesheet" media="screen" href="css/ie1.css">
		<![endif]-->
	</head>
	<body class="page1" id="top">
		<header>
			<!--==============================
			Logo
			=================================-->
			<section id="stuck_container">
				<div class="container">
					<div class="row">
						<div class="grid_12">
							<h1><a href=""> <img src="images/logo.png" alt="Logo alt"> </a></h1>
						</div>
					</div>
				</div>
			</section>
		</header>
		<!--=====================
		Content
		======================-->
		<section class="content">
			<div class="ic">
			</div>
			<div class="container">
				<div class="row">
					<div class="grid_12">
						<div class="ta__center">
							<h2>Login</h2>
							<div class="st1" style="font-size: 15px;font-style: italic;">
								Vul hieronder uw "Gebruikersnaam" en "Wachtwoord" in en klik op "Inloggen"
							</div>
							<center>
								<div class="banners">
									<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" name="inlog">
										<table width="200" border="0" style="width: 400px;background: white;">
											<tr>
												<td style="background: white; border: 0px;">Username</td>
												<td style="background: white; border: 0px;"><span id="username"> <label for="username"></label>
													<input type="text" name="username" id="username" />
											</tr>
											<tr>
												<td style="background: white; border: 0px;">Password</td>
												<td style="background: white; border: 0px;"><span id="pass"> <label for="pass"></label>
													<input type="password" name="pass" id="pass" />
											</tr>
											<tr style="background: white;">
												<td style="background: white; border: 0px;">&nbsp;</td>
												<td style="background: white; border: 0px;">
												<input type="submit" name="login" id="button" value="Log in" class="butt" />
												</td>
											</tr>
										</table>
									</form>
									<?php
									//Login Script
									if (isset($_POST['login'])) {
										$errors = array();

										If (empty($_POST['username'])) {
											array_push($errors, "U hebt geen username ingevuld!");
										} else {
											$username = $_POST['username'];
										}

										If (empty($_POST['pass'])) {
											array_push($errors, "U hebt geen wachtwoord ingevuld!");
										} else {
											$pass = $_POST['pass'];
										}

										If (empty($errors)) {
											$query_login_check = "SELECT * FROM logindata WHERE gebruikersnaam='$username' AND wachtwoord='$pass'";
											$result_login_check = mysqli_query($db, $query_login_check);
											$data_login_check = mysqli_fetch_array($result_login_check);

											$docid = $data_login_check['docid'];
											;
											$query_klas = "SELECT * FROM klassen WHERE docid='$docid'";
											$result_klas = mysqli_query($db, $query_klas);
											//$data_klas = mysqli_fetch_array($result_klas);

											if (!$result_login_check) {
												echo "<p style='color:red;'>Error in de database!</p>";
											}

											if (mysqli_num_rows($result_login_check) == 1) {
												$_SESSION['gebruikersid'] = $data_login_check['docid'];
												$_SESSION['gebruikersnaam'] = $username;
												$i = 1;
												while ($data_klas = mysqli_fetch_array($result_klas)) {
													$_SESSION['klasid_' . $i] = $data_klas['klasid'];
													$i++;
												}
												//$_SESSION['klasid'] = $data_klas['klasid'];
												$_SESSION['bevid'] = $data_login_check['bevid'];
												//$_SESSION['email'] = $email;
												//$_SESSION['pass'] = $pass;
												//$_SESSION['gebruikersnaam'] = $pass;
												header("Location: rc.php");
											} else {
												echo "<p style='color:red;'>U hebt niet de juiste gegevens ingevuld of uw account is niet actief.</p>";
											}
										} else {
											echo "<ol style='color:red;'>";
											foreach ($errors as $key => $values) {
												echo "<li>" . $values . "</li>";
											}
											echo "</ol>";
										}
										
										//Login gedeelte van de RC
										if($_SESSION['bevid'] == 4) {
											$docid = $_SESSION['gebruikersid'];
											$query_student = "SELECT * FROM richtings_coordinator INNER JOIN sectoren ON richtings_coordinator.secid = sectoren.secid WHERE richtings_coordinator.docid = '$docid'";
											$result_student = mysqli_query($db, $query_student);
											$data_student = mysqli_fetch_array($result_student);
											
											$_SESSION['rcid'] = $data_student['rcid'];
											$secid = $data_student['secid'];
											$_SESSION['secid'] = $secid;
											
											$query_sector = "SELECT * FROM sectoren WHERE secid = '$secid'";
											$result_sector = mysqli_query($db, $query_sector);
											$data_sector = mysqli_fetch_array($result_sector);
											
											$_SESSION['sector'] = $data_sector['sector'];
										}

										mysqli_close($db);
									}
									?>
								</div>
							</center>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--==============================
		footer
		=================================-->
		<?php
			include ("inc/footer.php");
 ?>
	</body>
</html>