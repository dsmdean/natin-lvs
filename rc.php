<?php session_start();
	include ("inc/db.php");
	
	$docid = $_SESSION['gebruikersid'];
	$query_naam_check = "SELECT * FROM docenten WHERE docid='$docid'";
	$result_naam_check = mysqli_query($db, $query_naam_check);
	$data_naam_check = mysqli_fetch_array($result_naam_check);
	
	$_SESSION['volNaam'] = $data_naam_check['voornaam']." ".$data_naam_check['naam'];
	
	if(!isset($_SESSION['gebruikersnaam'])) {
		header('Location: index.php');
	}?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>LVS | Home</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no">
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/stuck.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/ihover.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.1.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/tmStickUp.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script>
			$(document).ready(function(){
			 $().UItoTop({ easingType: 'easeOutQuart' });
			 $('#stuck_container').tmStickUp({});
			 });
		</script>
		<!--[if lt IE 9]>
		<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
			</a>
		</div>
		<script src="js/html5shiv.js"></script>
		<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
		<!--[if lt IE 10]>
		<link rel="stylesheet" media="screen" href="css/ie1.css">
		<![endif]-->
	</head>
	<body class="page1" id="top">
		<!--==============================
			header
			=================================-->
		<header>
			<!--==============================
				Stuck menu
				=================================-->
			<?php include ("inc/header.php"); ?>
		</header>
		<!--=====================
			Content
			======================-->
		<section class="content">
			<div class="ic">More Website Templates @ TemplateMonster.com - June 16, 2014!</div>
			<div class="container">
			<?php
			if($_SESSION['bevid'] == 4) {?>
				<div class="row">
					<div class="grid_12">
						<h3>Welkom <?php echo $_SESSION['volNaam'];?></h3>
					</div>
					<div class="grid_3" style="margin-left: 300px;">
						<div class="ih-item circle effect2 left_to_right">
							<a href="studenten.php">
								<div class="img"><img src="images/circ_img3.jpg" alt="img"></div>
								<div class="info">
									<h3>Studenten</h3>
									<p>studenten toevoegen</p>
								</div>
							</a>
						</div>
					</div>
					<div class="grid_3">
						<div class="ih-item circle effect2 left_to_right">
							<a href="klassen.php">
								<div class="img"><img src="images/circ_img4.jpg" alt="img"></div>
								<div class="info">
									<h3>Klassen</h3>
									<p>klassen toevoegen</p>
								</div>
							</a>
						</div>
					</div>
					<center>
						<div class="grid_3">
							<div class="ih-item circle effect2 left_to_right">
								<a href="presentie.php">
									<div class="img"><img src="images/circ_img5.jpg" alt="img"></div>
									<div class="info">
										<h3>Presentie</h3>
										<p>presentie toevoegen</p>
									</div>
								</a>
							</div>
						</div>
					</center>
				</div>		
			<?php
			} else if($_SESSION['bevid'] == 3) {?>
				<div class="row">
					<div class="grid_12">
						<h3>Welkom <?php echo $_SESSION['volNaam'];?></h3>
					</div>
					<div class="grid_3" style="margin-left: 300px;">
						<div class="ih-item circle effect2 left_to_right">
							<a href="studenten.php">
								<div class="img"><img src="images/circ_img3.jpg" alt="img"></div>
								<div class="info">
									<h3>Studenten</h3>
									<p>studenten toevoegen</p>
								</div>
							</a>
						</div>
					</div>
					<div class="grid_3">
						<div class="ih-item circle effect2 left_to_right">
							<a href="klassen.php">
								<div class="img"><img src="images/circ_img4.jpg" alt="img"></div>
								<div class="info">
									<h3>Klassen</h3>
									<p>klassen toevoegen</p>
								</div>
							</a>
						</div>
					</div>
					<center>
						<div class="grid_3">
							<div class="ih-item circle effect2 left_to_right">
								<a href="presentie.php">
									<div class="img"><img src="images/circ_img5.jpg" alt="img"></div>
									<div class="info">
										<h3>Presentie</h3>
										<p>presentie toevoegen</p>
									</div>
								</a>
							</div>
						</div>
					</center>
				</div>
			<?php
			} else if($_SESSION['bevid'] == 5 || $_SESSION['bevid'] == 6) {?>
				<div class="row">
					<div class="grid_12">
						<h3>Welkom <?php echo $_SESSION['volNaam'];?></h3>
					</div>
					<div class="grid_3" style="margin-left: 400px;">
						<div class="ih-item circle effect2 left_to_right">
							<a href="studenten.php">
								<div class="img"><img src="images/circ_img3.jpg" alt="img"></div>
								<div class="info">
									<h3>Studenten</h3>
									<p>studenten toevoegen</p>
								</div>
							</a>
						</div>
					</div>
					<div class="grid_3">
						<div class="ih-item circle effect2 left_to_right">
							<a href="klassen.php">
								<div class="img"><img src="images/circ_img4.jpg" alt="img"></div>
								<div class="info">
									<h3>Klassen</h3>
									<p>klassen toevoegen</p>
								</div>
							</a>
						</div>
					</div>
				</div>
			<?php
			}
			?>
			</div>
		</section>
		<!--==============================
			footer
			=================================-->
		<?php include ("inc/footer.php"); ?>
	</body>
</html>