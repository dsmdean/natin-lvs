<?php session_start();
include ("inc/db.php");

if (!isset($_SESSION['gebruikersnaam'])) {
	header('Location: index.php');
}

if(isset($_POST['opslaan'])) {
	if(isset($_POST['reden'])) {
		$reden = $_POST['reden'];
	} else {
		$reden = null;
	}
	if(isset($_POST['inhaal'])) {
		$inhaal = $_POST['inhaal'];
	} else {
		$inhaal = null;
	}
	if(isset($_POST['afgehandeld'])) {
		$afgehandeld = $_POST['afgehandeld'];
	} else {
		$afgehandeld = null;
	}
	
	$overzichtId = $_POST['overzichtId'];
	
	$overzichtUpdate = mysqli_query($db, "UPDATE niet_verzorgd SET reden='$reden', inhaal='$inhaal', afgehandeld='$afgehandeld' WHERE idniet_verzorgd='$overzichtId'");
	mysqli_close($db);
	
	$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>LVS | Niet verzorgde lessen</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no">
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="css/stuck.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/ihover.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.1.1.js"></script>
		<script src="js/script.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.mobilemenu.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/tmStickUp.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script type="text/javascript" src="js/css-pop.js"></script>
		<script>
			$(document).ready(function() {
				$().UItoTop({
					easingType : 'easeOutQuart'
				});
				$('#stuck_container').tmStickUp({});
			});
		</script>
		<!--[if lt IE 9]>
		<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
		<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
		</a>
		</div>
		<script src="js/html5shiv.js"></script>
		<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
		<!--[if lt IE 10]>
		<link rel="stylesheet" media="screen" href="css/ie1.css">
		<![endif]-->
	</head>
	<body class="page1" id="top" onload="popup('popUpDiv')">
		<!--==============================
		header
		=================================-->
		<header>
			<!--==============================
			Stuck menu
			=================================-->
			<?php
			include ("inc/header.php");
 ?>
		</header>
		<!--=====================
		Content
		======================-->
		<section class="content">
			<div class="ic">
				More Website Templates @ TemplateMonster.com - June 16, 2014!
			</div>

			<div class="container">
				<div >
					<p>
						<h1 style="font-size: 30px;">Overzicht Niet Verzorgde lessen</h1>
					</p>

					<?php
					$docentid = $_SESSION['gebruikersid'];
					$klasid = 0;
					if($_SESSION['bevid'] == 3) {
						$query_klassen_all = "SELECT * FROM klassen WHERE docid='$docentid'";
					} else if($_SESSION['bevid'] == 4) {
						$secid = $_SESSION['secid'];
						$query_klassen_all = "SELECT * FROM klassen WHERE secid='$secid'";
					}
					$result_klassen_all = mysqli_query($db, $query_klassen_all);
					$count_klassen_all = mysqli_num_rows($result_klassen_all);
					if(!isset($_GET['klas']) && $count_klassen_all > 1) {?>
						<div id="blanket" style="display:none;"></div>
						<div id="popUpDiv" style="display:none; top:250px;">			
						<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="GET">
							<div style="background:rgb(16, 70, 16); color: white;font-size: 20px;padding: 10px;font-weight: bold;"> <center>Kies een klas</center> </div>	
							<center>
								<div style="margin-top: 20px;">Klas kiezen: 
									<select name="klas">
										<?php
										while($data_klassen_all = mysqli_fetch_array($result_klassen_all)) {?>
											<option value="<?php echo $data_klassen_all['klasid']; ?>"><strong><?php echo $data_klassen_all['klas']; ?></strong></option>
										<?php
										}
										?>
									</select>
								</div>
							</center>
							<div style ="float: right; margin-right:50px; margin-top: 10px;"> 
								<input type="submit" value="Kiezen" onclick="popup('popUpDiv')" style="width: 70px; height: 35px; padding-left: 20px;padding-right: 20px;padding-top: 5px;padding-bottom: 5px;background: rgb(16, 70, 16); color: white;border: 0px;" >
							</div>			
						</form>
						</div>
					<?php	
					} else if(!isset($_GET['klas']) && $count_klassen_all == 1) {
						$data_klassen_all = mysqli_fetch_array($result_klassen_all);
						$klasid = $data_klassen_all['klasid'];
						header('Location: overzicht.php?klas='.$klasid);
					} else if(isset($_GET['klas'])) {
						$klasid = $_GET['klas'];
					}
					$query_docent = "SELECT * FROM docenten WHERE docid='$docentid'";
					$result_docent = mysqli_query($db, $query_docent);
					$data_docent = mysqli_fetch_array($result_docent);

					$query_dagstaat = "SELECT * FROM presentie WHERE presentie.klasid='$klasid' AND (presentie.les_1=2 OR presentie.les_2=2 OR presentie.les_3=2 OR presentie.les_4=2 OR presentie.les_5=2 OR presentie.les_6=2 OR presentie.les_7=2 OR presentie.les_8=2)";
					$result_dagstaat = mysqli_query($db, $query_dagstaat);

					if (mysqli_num_rows($result_dagstaat) == 0) {
						//header('location: dagbekijk.php');
					}
					$query_klas = "SELECT * FROM klassen INNER JOIN leerjaren ON klassen.ljid = leerjaren.ljid WHERE klasid='$klasid'";
					$result_klas = mysqli_query($db, $query_klas);
					$data_klas = mysqli_fetch_array($result_klas);
					?>

					<strong>Klas: <?php echo $data_klas['klas']; ?></strong>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Richting: ICT</strong>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Leerjaar: <?php echo $data_klas['leerjaar']; ?></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<strong><?php if($_SESSION['bevid'] == 3) { echo "Klassendocent: "; } else if($_SESSION['bevid'] == 4) { echo "Rictings Coordintor: "; } echo $data_docent['voornaam'] . " " . $data_docent['naam']; ?></strong>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<br>
					<br>

					<table border="1">

						<tr>
							<th>Datum</th>
							<th>Week</th>
							<th>Vak</th>
							<th>Lesuren</th>
							<th>Docent</th>
							<th>Rede Afwijking</th>
							<th>Inhaal datum</th>
							<th>Datum Afgeh.</th>
							<th></th>
						</tr>
						
						<?php
						$datums = array();
						$gemisteLes = array();
						$gemisteVakken = array();
						$gemisteDocenten = array();
						$presentieId = array();
						
						while($data_dagstaat = mysqli_fetch_array($result_dagstaat)) {
							$lesurenNietGevolgd = array();
							$docentNietGekomen = array();
							$vakNietGevolgd = array();
							$presId = array();
								
							for($jj=1;$jj<=8;$jj++) {
							  if($data_dagstaat['les_'.$jj] == 2) {
							  	array_push($lesurenNietGevolgd, $jj);
							  	
								array_push($docentNietGekomen, $data_dagstaat['docid_'.$jj]);
								
								array_push($vakNietGevolgd, $data_dagstaat['vak_'.$jj]);
								
								array_push($presId, $data_dagstaat['presid']);
							  }
							}
							
							
							array_push($datums, array($data_dagstaat['datum'], $data_dagstaat['week']));
							array_push($gemisteLes, $lesurenNietGevolgd);
							array_push($gemisteVakken, $vakNietGevolgd);
							array_push($gemisteDocenten, $docentNietGekomen);
							array_push($presentieId, $presId);
						}
							
							
							for($i=0;$i<count($datums);$i++) {
								//for($ii=0;$ii<count($gemisteLes[$i]);$ii++) {
									$overDatum = $datums[$i][0];
									$overPresid = $presentieId[$i][0];
									 
									$query_overzicht = "SELECT * FROM niet_verzorgd INNER JOIN presentie ON niet_verzorgd.presid = presentie.presid WHERE presentie.presid='$overPresid'";
									$result_overzicht = mysqli_query($db, $query_overzicht);
									$ii = 0;
									while($data_overzicht = mysqli_fetch_array($result_overzicht)) {
									?>
									<form action="<?php curPageURL(); ?>" method="POST">
										<input type="hidden" name="overzichtId" value="<?php echo $data_overzicht['idniet_verzorgd'];?>">
									<tr>
										<td>
											<strong>
											<?php echo $datums[$i][0]; ?>
											</strong>
										</td>
										
										<td>
											<strong>
											<?php echo $datums[$i][1]; ?>
											</strong>
										</td>
			
										<td>
											<strong>
											<?php echo $gemisteVakken[$i][$ii]; ?>
											</strong>
										</td>
			
										<td>
											<strong>
											<?php echo $gemisteLes[$i][$ii]; ?>
											</strong>
										</td>
			
										<td>
											<strong>
											<?php 
											$voorlDocentId = $gemisteDocenten[$i][$ii];
											$query_docent = "SELECT * FROM docenten WHERE docid='$voorlDocentId'";
											$result_docent = mysqli_query($db, $query_docent);
											$data_docent = mysqli_fetch_array($result_docent);
											echo $data_docent['voornaam'] . " " . $data_docent['naam']; ?>
											</strong>
										</td>
			
										<td>
										<?php
										
										
										if($data_overzicht['reden'] != NULL) {
											echo $data_overzicht['reden'];?>
											<input type="hidden" name="reden" value="<?php echo $data_overzicht['reden'];?>">
										<?php
										} else {?>
											<input type="text" name ="reden" placeholder="Reden">
										<?php
										}?>
										</td>
			
										<td>
										<?php
										if($data_overzicht['inhaal'] != NULL) {
											echo $data_overzicht['inhaal'];?>
											<input type="hidden" name="inhaal" value="<?php echo $data_overzicht['inhaal'];?>">
										<?php
										} else {?>
											<input type="date" name="inhaal">
										<?php
										}?>
										</td>
										<td>
										<?php
										if($data_overzicht['afgehandeld'] != NULL) {
											echo $data_overzicht['afgehandeld'];?>
											<input type="hidden" name="afgehandeld" value="<?php echo $data_overzicht['afgehandeld'];?>">
										<?php
										} else {?>
											<input type="date" name="afgehandeld">
										<?php
										}?>
										</td>
										<td>
										<input type="submit" value="Opslaan" name="opslaan">
										</td>
									</tr>
									</form>
							<?php
							$ii++;
							}?>
							<tr>
								<td>
								</td>
								<td>
								</td>
								<td>
								</td>
								<td>
								</td>
								<td>
								</td>
								<td>
								</td>
								<td>
								</td>
								<td>
								</td>
								<td>&nbsp;
								</td>
							</tr>
						<?php
						}?>
					</table>
				</div>
			</div>
		</section>
		<!--==============================
		footer
		=================================-->
		<?php
		include ("inc/footer.php");
 ?>
	</body>
</html>

